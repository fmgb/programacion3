package modelo;


/**
 * La Clase Movimiento Ordinario. Hereda de la Clase Movimiento. Sustituye a la Clase Movimiento en la anterior practica.
 * 
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class MovimientoOrdinario extends Movimiento {

	/**
	 * Constructor de Movimiento Ordinario.
	 *
	 * @param co La coordenada origen que le pasamos.
	 * @param cd La coordenada destino que le pasamos.
	 */
	public MovimientoOrdinario(Coordenada co, Coordenada cd) {
		super(co, cd);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MovimientoOrdinario [co=" + co + ", cd=" + cd + "]";
	}
}
