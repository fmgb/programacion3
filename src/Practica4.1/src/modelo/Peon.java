
package modelo;

import java.util.HashSet;
import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimientoIlegal;

/**
 * La clase Peon. Hereda de la clase Pieza.
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class Peon extends Pieza {
	
	/** El tipo de la pieza. */
	public static final char tipo = 'P';

	/**
	 * Constructor del peon.
	 *
	 * @param c El color que sera el peon.
	 */
	public Peon(Color c) {
		super(c); // Pasamos al padre el color.
		this.valor = 1.0;
	}

	/**
	 * Comprueba que el peon esta en la casilla inicial de la partida. BLANCOS en la 2 y NEGROS en la 7.
	 *
	 * @return Cierto, si esta en la posicion, false si no.
	 */
	public boolean isEnPosicionOriginal() { 
		boolean esta = false;
		if(this.getColor() == Color.NEGRO && this.getCasilla().getCoordenada().getY() == 7) {
			esta = true;
		}
		if(this.getColor() == Color.BLANCO && this.getCasilla().getCoordenada().getY() == 2) {
			esta = true;
		}
		return esta;
	}
	
	/* (non-Javadoc)
	 * @see modelo.Pieza#isMismoTipo(modelo.Pieza)
	 */
	@Override
	public boolean isMismoTipo(Pieza p) {
		boolean is = false;

		if (p.getTipo() == tipo) { // Si es del mismo tipo devolvemos true.
			is = true;
		}
		return is;
	}
	
	/**
	 * Getter de Tipo.
	 *
	 * @return El tipo de la pieza.
	 */
	public char getTipo() {
		return tipo;
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#puedeMover(modelo.Casilla)
	 */
	@Override
	public boolean puedeMover(Casilla c) {
		boolean puede = false;
		if(c.isNula() || this.getCasilla() == null) {
			return puede;
		} else { //ELSE NULA
			if(this.getColor() == Color.BLANCO) { // If Color
				if(this.isEnPosicionOriginal()) {// IF posicionOriginal
					try {
						if(puedeDesdeOriginalB(c)) { 
							puede = true;
						}
					} catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage());
					}
					
				}//FIn If posicionOriginal
				if(this.puedeDesdeNormalB(c) ) { //|| c.getPieza() == this) {
					puede = true;
				}
			}//FIN If Color.
			else if(this.getColor() == Color.NEGRO) { // ELSE IF NEGRO
				if(this.isEnPosicionOriginal()) {
					try {
						if(puedeDesdeOriginalN(c)) {
							puede = true;
						}
					} catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage());
					}
				}
				if(this.puedeDesdeNormalN(c) ) { //|| c.getPieza() == this) {
					puede = true;
				}
			}//FIN ELSE IF NEGRO
			if(puedeMatar(c)) {
				puede = true;
			}
		}//FIN ELSE NULA
		return puede;
	}//FIN puedeMover

	/**
	 * Metodo auxiliar para comprobar que puede desde la casilla Inicial. COLOR NEGRO.
	 *
	 * @param c La casilla a la que se va a mover.
	 * @return Cierto, si puede. False si no puede.
	 * @throws ExcepcionCoordenadaErronea La excepcion coordenada erronea
	 */
	private boolean puedeDesdeOriginalN(Casilla c) throws ExcepcionCoordenadaErronea  {
		boolean puede = false;
		int Yc = c.getCoordenada().getY();
		int Yp = this.getCasilla().getCoordenada().getY();
		char Lp = this.getCasilla().getCoordenada().getLetra();
		char Lc = c.getCoordenada().getLetra();
		
		if(Yc == (Yp - 2) && Lc == Lp) { // If Coordenadaas correctas.
			if(!c.isOcupada() && !T.getCasillaAt(Lc,Yc+1).isOcupada()) { //If no están ocupadas
				puede = true;
			}
		}
		return puede;
	}//Fin PuedeDesdeOriginalN
	
	/**
	 * Metodo auxiliar para comprobar que puede desde la casilla Inicial. COLOR BLANCO.
	 *
	 * @param c La casilla a la que se va a mover.
	 * @return Cierto, si puede. False si no puede.
	 * @throws ExcepcionCoordenadaErronea La excepcion coordenada erronea
	 */
	private boolean puedeDesdeOriginalB(Casilla c) throws ExcepcionCoordenadaErronea  {
		boolean puede = false;
		int Yc = c.getCoordenada().getY();
		int Yp = this.getCasilla().getCoordenada().getY();
		char Lp = this.getCasilla().getCoordenada().getLetra();
		char Lc = c.getCoordenada().getLetra();
		
		if(Yc == (Yp + 2) && Lc == Lp) { // If Coordenadaas correctas.
			if(!c.isOcupada() && !T.getCasillaAt(Lc,Yc-1).isOcupada()) {
				puede = true;
			}
		}
		return puede;
	}//FIN PuedeDesdeOriginalB
	
	/**
	 * Puede mover desde cualquier otra posicion.
	 *
	 * @param c La casilla que se va a mover. COLOR BLANCO
	 * @return Cierto si puede. Falso si no.
	 */
	private boolean puedeDesdeNormalB(Casilla c) {
		boolean puede = false;
		int Yc = c.getCoordenada().getY();
		int Yp = this.getCasilla().getCoordenada().getY();
		char Lp = this.getCasilla().getCoordenada().getLetra();
		char Lc = c.getCoordenada().getLetra();
		
		if(Lc == Lp &&  Yc == (Yp + 1)) { // c.Y == casilla.Y+1
			if(!c.isOcupada()) {
				puede = true;
			}
		} 
		if((Lc == Lp + 1 || Lc == Lp - 1) && (Yc == (Yp + 1))) {// c.Y == casilla.Y+1
			if(c.isOcupada() && !this.isMismoColor(c.getPieza())) {
				puede = true;
			}
		}
		return puede;
	}//FIN puedeDesdeNormalB
	
	/**
	 * Puede mover desde cualquier otra posicion.
	 *
	 * @param c La casilla que se va a mover. COLOR NEGRO
	 * @return Cierto si puede. Falso si no.
	 */
	private boolean puedeDesdeNormalN(Casilla c) {
		boolean puede = false;
		int Yc = c.getCoordenada().getY();
		int Yp = this.getCasilla().getCoordenada().getY();
		char Lp = this.getCasilla().getCoordenada().getLetra();
		char Lc = c.getCoordenada().getLetra();
		
		if(Lc == Lp && Yc == (Yp-1)) { // c.Y == casilla.Y-1
			if(!c.isOcupada()) {
				puede = true;
			}
		} //FIN If coordenadas
		if((Lc == Lp + 1 || Lc == Lp - 1) && Yc == (Yp -1)) {// c.Y == casilla.Y-1
			if(c.isOcupada() && !this.isMismoColor(c.getPieza())) {
				puede = true;
			}
		}
		return puede;
	}//FIN puedeDesdeNormalN
	

	/**
	 * Puede matar.
	 *
	 * @param c La casilla que va a matar.
	 * @return Cierto si puede. Falso si no.
	 */
	private boolean puedeMatar(Casilla c) {
		if(this.getCasillasAmenazadas().contains(c) && c.isOcupada() && !this.isMismoColor(c.getPieza())) {
			return true;
		}
		return false;
		
	}
	
	/* (non-Javadoc)
	 * @see modelo.Pieza#mueve(modelo.Casilla)
	 */
	@Override
	public void mueve(Casilla c) throws ExcepcionMovimientoIlegal, ExcepcionCasillaDestinoOcupada{ 
		if(this.getCasilla() == null) { 
			System.out.println("La pieza no ha sido colocada en una casilla");
			} else { //ELSE NULL
				Movimiento m = new MovimientoOrdinario(this.getCasilla().getCoordenada(), c.getCoordenada()); //Perfecto
				if(m.getCoordenadaOrigen() == m.getCoordenadaDestino()) {
					throw new ExcepcionMovimientoIlegal(this, m);
				}
				if(c.getPieza()!= null) {
					int Yp = this.getCasilla().getCoordenada().getY();
					int Yc = c.getCoordenada().getY();
					char Lp = this.getCasilla().getCoordenada().getLetra();
					char Lc = c.getCoordenada().getLetra();
					if((Lc == (Lp-1) && Yc == Yp +1 && c.isOcupada()) || Lc == (Lp+1) && Yc == Yp+1 && c.isOcupada()) {
					
					} else if((Lc == (Lp-1) && Yc == Yp -1 && c.isOcupada()) || Lc == (Lp+1) && Yc == Yp-1 && c.isOcupada()) {
						
					}
					else if (c.isOcupada() && !this.isMismoColor(c.getPieza())){
						throw new ExcepcionCasillaDestinoOcupada(m);
					}
				}
				if(this.isEnPosicionOriginal()) {
					if(this.getColor() == Color.BLANCO){
						if(this.getCasilla().getCoordenada().getY() + 2 == c.getCoordenada().getY() && this.getCasilla().getCoordenada().getLetra() == c.getCoordenada().getLetra()) {
							try {
								if(T.getCasillaAt(c.getCoordenada().getLetra(), c.getCoordenada().getY() -1).isOcupada()) {
									throw new ExcepcionCasillaDestinoOcupada(null);
								}
							} catch (ExcepcionCoordenadaErronea e) {
								System.err.println(e.getMessage());
							}
						}
					}
				}
				if(!puedeMover(c)) {
					throw new ExcepcionMovimientoIlegal(this, m);
				}
				else {//ELSE
					if(c.getPieza() != null) {
						c.quitaPieza();
					}
					this.quitaDeCasilla();
					c.setPieza(this);
					this.HaMovido = true;
				}//FIN ELSE
			}//FIN ELSE NULL
	}//FIN MUEVE
	
	/**
	 * Comprueba si esta en la ultima fila. BLANCOS 8 y NEGROS 1.
	 *
	 * @return Cierto si esta en su ultima fila. Falso si es lo contrario.
	 */
	public boolean isEnUltimaFila() {
		if(this.getCasilla()!= null) {
		int Y = this.getCasilla().getCoordenada().getY();
		if(this.getColor() == Color.NEGRO) {
			if(Y == 1) {
				return true;
			}
		}
		else if (this.getColor() == Color.BLANCO)
		{
			if(Y == 8) {
				return true;
			}
		}
		else {
			return false;
		}
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see modelo.Pieza#getCasillasAmenazadas()
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas() {
		Set <Casilla> Lista = new HashSet<Casilla> ();
		
		if(this.getCasilla() == null) { //TODO Si la Casilla es Color NULO.
			System.err.println("La Casilla de esta pieza : " + this.toString() + " es nula.");
		} else {
			char Letra = this.getCasilla().getCoordenada().getLetra();
			int Y = this.getCasilla().getCoordenada().getY();
			try {
				if(this.getColor() == Color.BLANCO){
					if(T.casillaEstaEnTablero((char) (Letra - 1), Y + 1)) {
							Lista.add(T.getCasillaAt((char) (Letra - 1), Y + 1));
					}
					if(T.casillaEstaEnTablero((char) (Letra +1), Y + 1)) {
						Lista.add(T.getCasillaAt((char) (Letra + 1),Y + 1));
					}
				}
				else if( this.getColor() == Color.NEGRO){
					if(T.casillaEstaEnTablero((char) (Letra - 1), Y - 1)) {
						Lista.add(T.getCasillaAt((char) (Letra - 1), Y - 1));
					}
					if(T.casillaEstaEnTablero((char) (Letra +1), Y - 1)) {
					Lista.add(T.getCasillaAt((char) (Letra + 1),Y - 1));
					}
				}
			} catch (ExcepcionCoordenadaErronea e) {
				System.err.println(e.getMessage());
			}
		}//ELSE == null
		return Lista;
	} // FIN GetCasillasAmenazadas

}//FIN CLASE
