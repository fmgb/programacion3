/**
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
package modelo;

import java.util.ArrayList;
import java.util.List;

import modelo.excepciones.ExcepcionCoordenadaErronea;

/**
 * La Clase Casilla.
 * 
 * @author Francisco Manuel García Botella 48570019-E
 */
public class Casilla {

	/** La casilla Error. */
	public static Casilla CasillaError = new Casilla();

	/** La coordenada. */
	private Coordenada coordenada;

	/** La pieza. */
	private Pieza pieza;

	/** El color. */
	private Color color;

	/**
	 * Constructor Crea una casilla vacía con el color y coordenada dados.
	 * 
	 * @param col Valor del color de la casilla.
	 * @param cor Valor de la coordenada.
	 */
	public Casilla(Color col, Coordenada cor) {
		color = col;
		coordenada = new Coordenada(cor);
	}

	/**
	 * Constructor Crea una casilla nula(Color nulo), vacía y coordeanda ('0',0).
	 *
	 */
	public Casilla() {
		color = Color.NULO;
		coordenada = new Coordenada();
		pieza = null;
	}

	/**
	 * Metodo Comprueba si la casilla esta ocupada.
	 * 
	 * @return Devuelve cierto si la casilla tiene una pieza, sino devuelve false.
	 */
	public boolean isOcupada() {
		if (pieza != null) // Si no es null es porque tiene una pieza.
		{
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Metodo Compueba si una casilla es nula.
	 * 
	 * @return Devuelve cierto si la casilla tiene color NULO.
	 */
	public boolean isNula() {
		if (color == Color.NULO) // Es nula si contiene el color NULO.
		{
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Metodo Coloca una pieza en esta casilla.
	 * 
	 * @param p Le pasamos la pieza que queremos colocar.
	 * @return Devuelve true si ha podido colocar la pieza, sino false.
	 */
	public boolean setPieza(Pieza p) { 
		
		if (p.getCasilla() == this && p.isValida()) {
			if(pieza == null) {
				pieza = p;
			}
			return true;
		}

		if (!isNula() && !isOcupada() && p.isValida() && p.getCasilla() == null) {
			pieza = p;
			if (p.getCasilla() != this) { // caso base enlace bi
				p.setCasilla(this); // enlace bi
			}
			return true;
		} else {
			if (p.getCasilla() == this) {
				return true;
			}
			return false;
		}

	}

	/**
	 * Metodo que quita la pieza de la casilla y la devuelve.
	 * 
	 * @return Si hay pieza devuelve la pieza y sino devuelve null.
	 */
	public Pieza quitaPieza() {
		Pieza aux;

		if (pieza == null) // Si no hay pieza devuelve null.
		{
			return null;
		} else // Sino devuelve la pieza que quita.
		{

			aux = pieza;
			pieza.quitaDeCasillaAux();

			pieza = null; // Ponemos la pieza a null, porque la quitamos.
			
			return aux; // Devolvemos la pieza que hemos quitado.
		}
	}
	
	/**
	 * Quita pieza aux.
	 */
	public void quitaPiezaAux() {
		if (pieza != null) {
			pieza = null;
		}
	}

	/**
	 * Getter.
	 *
	 * @return Devuelve la pieza de la casilla.
	 */
	public Pieza getPieza() {
		return pieza;
	}

	/**
	 * Getter Color de la Casilla.
	 * 
	 * @return Devuelve el color de una casilla.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Setter que modifica el color de la casilla mediante el parametro dado.
	 * 
	 * @param C Color que le pasamos al metodo
	 */
	public void setColor(Color C) {
		if (C == Color.BLANCO || C == Color.NEGRO || C == Color.NULO) {
			color = C;
		}
	}

	/**
	 * Getter Coordenada de la Casilla.
	 * 
	 * @return Devuelve la coordenada de la casilla.
	 */
	public Coordenada getCoordenada() {
		if(coordenada == null){
			return null;
		}else {
		return coordenada;
		}
	}

	/**
	 * Metodo que modifica una coordenada con los parametros que le pasamos.
	 *
	 * @param c Caracter que indica la posicion x del tablero.
	 * @param y Entero que indica la posicion y del tablero.
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public void setCoordenada(char c, int y) throws ExcepcionCoordenadaErronea {
		coordenada = new Coordenada(c, y);
	}
	
	/**
	 * Comprueba si una casilla esta amenazada por una pieza.
	 *
	 * @param c La casilla que se comprobara.
	 * @return True, si esta amenazada. False si es lo contrario.
	 */
	public boolean isAmenazada(Color c) {
		List<Pieza> ListaP = new ArrayList<Pieza>();
		PartidaAjedrez paj = PartidaAjedrez.getInstancia();
        ListaP = paj.getTablero().getAmenazas(this, c);
		if(!ListaP.isEmpty()){
			return true;
		}else {
			return false;
		}
	}

	/**
	 * Equals de Casilla. Verifica si el objeto que le pasamos tiene las mismas caracteristicas de la Casilla anfitriona. Ademas de las mismas caracteristicas mira si son los mismos valores.
	 * 
	 * @param obj El objeto que le pasamos.
	 * @return true si son iguales las dos casillas.
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;

		if (obj == null)
			return false;

		if (!(obj instanceof Casilla))
			return false;

		Casilla otro = (Casilla) obj;

		if (otro.color != color)
			return false;

		if (otro.pieza != pieza)
			return false;

		if (!otro.coordenada.equals(coordenada))
			return false;

		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Casilla [coordenada=" + coordenada + ", pieza="
				+ ", color=" + color + "]";
	}

}
