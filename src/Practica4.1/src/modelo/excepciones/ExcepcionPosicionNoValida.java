
package modelo.excepciones;

import modelo.Coordenada;

/**
 * La clase ExcepcionPosicionNoValida.
 * @author Francisco Manuel García Botella 48570019-E
 */
@SuppressWarnings("serial")
public class ExcepcionPosicionNoValida extends Exception {
	
	/** La coordenada. */
	private Coordenada coord;

	/**
	 * Constructor de la Excepcion.
	 *
	 * @param c La coordenada que le pasamos.
	 */
	public ExcepcionPosicionNoValida(Coordenada c) {
		coord = new Coordenada(c);
	}

	/**
	 * Getter de la coordenada.
	 *
	 * @return La coordenada.
	 */
	public Coordenada getCoordenada() {
		return coord;
	}

	/**
	 * getMessage El mensaje.
	 *
	 * @return String del mensaje de la excepcion
	 */
	public String getMessage() {
		return "Ya hay otra pieza en la coordenada: x= " + coord.getLetra() + ", y= " + coord.getY();
	}
}
