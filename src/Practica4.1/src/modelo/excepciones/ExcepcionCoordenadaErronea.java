package modelo.excepciones;


/**
 * La Clase ExcepcionCoordenadaErronea.
 * @author Francisco Manuel García Botella 48570019-E
 */
@SuppressWarnings("serial")
public class ExcepcionCoordenadaErronea extends Exception {

	/** The letra. */
	private char letra;
	
	/** The y. */
	private int y;

	/**
	 * Instantiates a new excepcion coordenada erronea.
	 *
	 * @param le the le
	 * @param y the y
	 */
	public ExcepcionCoordenadaErronea(char le, int y) {
		letra = le;
		this.y = y;
	}

	/**
	 * Gets the letra.
	 *
	 * @return the letra
	 */
	public char getLetra() {
		return letra;
	}

	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * getMessage El mensaje que lanzara la excepcion.
	 *
	 * @return String del mensaje.
	 */
	public String getMessage() {
		return "Coordenada(x,y) errónea: x =" + letra + ", y = " + y;
	}
}
