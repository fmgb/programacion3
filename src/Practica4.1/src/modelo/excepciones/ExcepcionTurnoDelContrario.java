package modelo.excepciones;

import modelo.Movimiento;

/**
 * La Clase ExcepcionTurnoDelContrario.
 * @author Francisco Manuel García Botella 48570019-E
 */
@SuppressWarnings("serial")
public class ExcepcionTurnoDelContrario extends ExcepcionMovimiento {
	
	/**
	 * Instantiates a new excepcion turno del contrario.
	 *
	 * @param mov the mov
	 */
	public ExcepcionTurnoDelContrario(Movimiento mov) {
		super(mov);
	}

	/**
	 * getMessage El mensaje.
	 *
	 * @return String del mensaje de la excepcion
	 */
	public String getMessage() {
		return "No se ha podido efectuar el movimiento porque El turno es del jugador contrario";
	}
}
