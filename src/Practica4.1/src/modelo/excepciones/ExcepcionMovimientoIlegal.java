package modelo.excepciones;

import modelo.Movimiento;
import modelo.Pieza;


/**
 * La Clase ExcepcionMovimientoIlegal.
 * @author Francisco Manuel García Botella 48570019-E
 */
@SuppressWarnings("serial")
public class ExcepcionMovimientoIlegal extends ExcepcionMovimiento {

	/** The p. */
	private Pieza p;

	/**
	 * Instantiates a new excepcion movimiento ilegal.
	 *
	 * @param p the p
	 * @param m the m
	 */
	public ExcepcionMovimientoIlegal(Pieza p, Movimiento m) {
		super(m);
		this.p = p;
	}

	/**
	 * Gets the pieza.
	 *
	 * @return the pieza
	 */
	public Pieza getPieza() {
		return p;
	}

	/**
	 * getMessage El mensaje.
	 *
	 * @return String del mensaje de la excepcion
	 */
	public String getMessage() {
		return "La pieza " + p.getTipo() + p.getColor() + " no puede realizar el movimiento de ("
				+ this.getMovimiento().getCoordenadaOrigen().toString()
				+ ") a ("
				+ this.getMovimiento().getCoordenadaDestino().toString() + ").";
	}
}
