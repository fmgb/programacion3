package modelo.excepciones;

import modelo.Movimiento;


/**
 * La Clase ExcepcionMovimiento.
 * @author Francisco Manuel García Botella 48570019-E
 */
@SuppressWarnings("serial")
public abstract class ExcepcionMovimiento extends Exception{
	
	/** The mov. */
	private Movimiento mov;

	/**
	 * Instantiates a new excepcion movimiento.
	 *
	 * @param m the m
	 */
	public ExcepcionMovimiento(Movimiento m) {
		this.mov = m;
	}

	/**
	 * Gets the movimiento.
	 *
	 * @return the movimiento
	 */
	public Movimiento getMovimiento() {
		return mov;
	}

}
