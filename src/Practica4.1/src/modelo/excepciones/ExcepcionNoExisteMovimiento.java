package modelo.excepciones;

/**
 * La Clase ExcepcionNoExisteMovimiento.
 * @author Francisco Manuel García Botella 48570019-E
 */
@SuppressWarnings("serial")
public class ExcepcionNoExisteMovimiento extends ExcepcionMovimiento {
	
	/** The indice. */
	private int indice;

	/**
	 * Instantiates a new excepcion no existe movimiento.
	 *
	 * @param indice the indice
	 */
	public ExcepcionNoExisteMovimiento(int indice) {
		super(null);
		this.indice = indice;
	}

	/**
	 * getMessage El mensaje.
	 *
	 * @return String del mensaje de la excepcion
	 */
	public String getMessage() {
		return "El Movimiento " + indice + "no existe";
	}

}
