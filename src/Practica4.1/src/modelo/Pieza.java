package modelo;

import java.util.Set;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimientoIlegal;


/**
 * La Clase Pieza.
 * 
 * @author Francisco Manuel García Botella 48570019-E
 */

public abstract class Pieza {

	/** El color de la Pieza. */
	private Color color;

	/** La casilla. */
	private Casilla casilla;
	
	/** Si Ha movido durante la partida. */
	protected boolean HaMovido = false;
	
	/** El valor de la pieza. */
	protected double valor;
	
	/** El tablero de la partida. */
	protected Tablero T = PartidaAjedrez.getInstancia().getTablero();
	
	/**
	 * Constructor.
	 * 
	 * @param c Crea una pieza del color que le hemos pasado.
	 */
	public Pieza(Color c) {
		color = c;
		casilla = null;		
	}

	/**
	 * Metodo Comprueba que las piezas son del mismo color.
	 * 
	 * @param p Le pasamos una pieza para comprobar si es del mismo color.
	 * @return Devuelve cierto si las dos piezas son del mismo color, falso si
	 *         es lo contrario.
	 */
	public boolean isMismoColor(Pieza p) {
		if (this.color == p.color) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Getter Color.
	 * 
	 * @return Devuelve el color de la pieza
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Gets the casilla.
	 *
	 * @return the casilla
	 */
	public Casilla getCasilla() {
		return casilla;
	}

	/**
	 * Metodo Comprueba si la pieza es valida.
	 * 
	 * @return Devuelve true si no es NULO, sino devuelve falso.
	 */
	public boolean isValida() {
		if (this.color != Color.NULO) {
			return true;
		} else {
			return false;
		}
	}

	// METODOS ABSTRACTOS.
	/**
	 * Comprueba si es del mismo tipo la pieza.
	 *
	 * @param p Pieza que se comprobara
	 * @return Cierto si es del mismo tipo. Falso si no es.
	 */
	public abstract boolean isMismoTipo(Pieza p);

	/**
	 * Get Casillas Amenazadas. 
	 * @return Devuelve las casillas que amenaza esta pieza.
	 */
	public abstract Set<Casilla> getCasillasAmenazadas();
	
	/**
	 * Getter de tipo.
	 *
	 * @return Devuelve el tipo de la pieza.
	 */
	public abstract char getTipo();

	/**
	 * Puede mover.
	 *
	 * @param c La casilla que se intentara mover la pieza.
	 * @return Cierto, si puede mover. Falso si es lo contrario.
	 */
	public boolean puedeMover(Casilla c) {
		if(c.isNula() || this.getCasilla() == null || c == this.getCasilla()) {
			return false;
		} else { //ELSE NULA
			if(this.getCasillasAmenazadas().contains(c) && (!c.isOcupada()) 
					|| (c.isOcupada() && !this.isMismoColor(c.getPieza()))) { 
				return true;
			}else {
				return false;
			}
		}//FIN ELSE NULA
	}//FIN puedeMover
	
	/**
	 * Mueve.
	 *
	 * @param c La casilla que movera la pieza.
	 * @throws ExcepcionCasillaDestinoOcupada Excepcion Casilla Destino Ocupada
	 * @throws ExcepcionMovimientoIlegal Excepcion Movimiento Ilegal
	 */
	public void mueve(Casilla c) throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal { 
		if(this.getCasilla() == null) { 
			System.out.println("La pieza no ha sido colocada en una casilla");
			} else {
				Movimiento m = new MovimientoOrdinario(this.getCasilla().getCoordenada(), c.getCoordenada());
				
				if(c.getPieza() != null){
					if(this.isMismoColor(c.getPieza()) && c.isOcupada()) {
						throw new ExcepcionCasillaDestinoOcupada(m);
					}
				}
				if(!puedeMover(c)) {
					throw new ExcepcionMovimientoIlegal(this, m);
				}
				else {
					if(c.getPieza() != null) {
						c.quitaPieza();
					}
					
					this.quitaDeCasilla();
					c.setPieza(this);
					this.HaMovido= true;
				}
			}
	}

	/**
	 * Set Casilla.
	 *
	 * @param c La casilla que se pondra la pieza.
	 * @return True si se ha añadido la casilla, false en caso contrario.
	 */
	public boolean setCasilla(Casilla c) {
		if(!isValida()) {
			return false;
		}
		if(casilla == null && !c.isNula()) {
			if(!c.isOcupada() || c.getPieza() == this) {
				casilla = c;
				casilla.setPieza(this);
				return true;
			}
			else {
				return false;
			}
		} else {
			if(c.getPieza() == this) {
				return true;
			}
			else {
				return false;
			}
		}
	}		
	
	/**
	 * Quita de casilla.
	 *
	 * @return La casilla que se ha quitado a la pieza. Si no tenia pieza, devuelve null.
	 */
	public Casilla quitaDeCasilla() {
		if(casilla == null) {
			return null;
		}
		else {
				Casilla aux = casilla;
				casilla.quitaPiezaAux();
				casilla = null;
				return aux;
		}
	}
	
	/**
	 * Metodo auxiliar para salir del bucle.
	 */
	public void quitaDeCasillaAux() {
		if( casilla != null) {
			casilla = null;
		}
	}

	/**
	 * To string.
	 *
	 * @return string del color de la pieza.
	 */
	@Override
	public String toString() {
		String s;
		s="Pieza [color= " + color +" Tipo= "+ this.getTipo()+" ] casilla="+this.getCasilla()+"]";
		return s;
	}

	/**
	 * Comprueba si la casilla esta en el tablero o no.
	 *
	 * @param c La casilla que se va a comprobar.
	 * @return Cierto si esta fuera del tablero. Falso si no.
	 */
	protected boolean isFueraTablero(Casilla c) {
		if(this.getCasilla() == null) {
			return true;
		}
		boolean isFuera = false;
		char Letrac = c.getCoordenada().getLetra();
		int Yc = c.getCoordenada().getY();
		char LetraP = this.getCasilla().getCoordenada().getLetra();
		int YP = this.getCasilla().getCoordenada().getY();
		if(Letrac > LetraP || Yc > YP)
		{
			isFuera = true;
		}
		return isFuera;
	}
	
	/**
	 * Setter de color.
	 *
	 * @param c El nuevo color de la pieza.
	 */
	public void setColor(Color c) {
		this.color = c;
	}
	
	/**
	 * Ha movido.
	 *
	 * @return Cierto si ha movido alguna vez esa pieza.
	 */
	public boolean haMovido() {
		return HaMovido;
	}
	
	/**
	 * Gets the valor.
	 *
	 * @return El valor que representa esa pieza en la partida.
	 */
	public double getValor() {
		return this.valor;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((casilla == null) ? 0 : casilla.hashCode());
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + (HaMovido ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(valor);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pieza other = (Pieza) obj;
		if (casilla == null) {
			if (other.casilla != null)
				return false;
		} else if (!casilla.equals(other.casilla))
			return false;
		if (color != other.color)
			return false;
		if (HaMovido != other.HaMovido)
			return false;
		if (Double.doubleToLongBits(valor) != Double
				.doubleToLongBits(other.valor))
			return false;
		return true;
	}
	
}
