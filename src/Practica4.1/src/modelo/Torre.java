package modelo;

import java.util.HashSet;
import java.util.Set;



/**
 * La clase Torre. Hereda de Pieza.
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class Torre extends Pieza {

	/** El tipo de la pieza. */
	private static final char tipo = 'T';
	
	/**
	 * Constructor de Torre.
	 *
	 * @param c El color que va a ser esa pieza.
	 */
	public Torre(Color c) {
		super(c);
		this.valor = 5.0;
	}
	
	/* (non-Javadoc)
	 * @see modelo.Pieza#isMismoTipo(modelo.Pieza)
	 */
	@Override
	public boolean isMismoTipo(Pieza p) {
		if(this.getTipo() == p.getTipo()) {
			return true;
		}else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#getCasillasAmenazadas()
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas() {
		Set<Casilla> Lista = new HashSet<Casilla> ();
		if(this.getCasilla() == null) {
			System.err.println("La casilla de la pieza: " + this.toString() + " es nula.");
		}else {
			char Letra = this.getCasilla().getCoordenada().getLetra();
			int Y = this.getCasilla().getCoordenada().getY();
			Lista = getCasillasAmenazadasTorre(Letra,Y);	
		}
		return Lista;
	}

	/**
	 * Getter las casillas amenazadas por la Torre.
	 *
	 * @param L La Letra de la coordenada donde esta la Torre.
	 * @param Y El numero de la coordenada donde esta la Torre.
	 * @return La lista de Casillas amenazadas por la Torre.
	 */
	private Set<Casilla> getCasillasAmenazadasTorre(char L, int Y) {
		Set<Casilla> Lista = new HashSet<Casilla>();
				Lista.addAll(T.getColumnaAbajo(this.getCasilla()));
				Lista.addAll(T.getColumnaArriba(this.getCasilla()));
				Lista.addAll(T.getFilaDerecha(this.getCasilla()));
				Lista.addAll(T.getFilaIzquierda(this.getCasilla()));
		return Lista;
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#getTipo()
	 */
	@Override
	public char getTipo() {
		return tipo;
	}

}
