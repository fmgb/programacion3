package modelo;

import java.lang.reflect.Constructor;
import java.util.Hashtable;

import modelo.excepciones.ExcepcionPiezaDesconocida;


/**
 * La Clase FactoriaPieza. Encargada de crear las piezas del tablero.
 *
 * @author Franciso Manuel García Botella 48570019-E
 */
public class FactoriaPieza {
	
	/** La tabla piezas. */
	static Hashtable<Character, String> tablaPiezas;	
	
	/**
	 * Inits the.
	 */
	private static void init() 
	{		
		if (tablaPiezas == null) {
		tablaPiezas = new Hashtable<Character, String>();
		tablaPiezas.put('A', "Alfil");
		tablaPiezas.put('C', "Caballo");
		tablaPiezas.put('P', "Peon");
		tablaPiezas.put('D', "Dama");
		tablaPiezas.put('R', "Rey");
		tablaPiezas.put('T', "Torre");
		}
	}
	
	/**
	 * Crea pieza en el tablero mediante la string(Mayuscula la primera letra) y el color.
	 *
	 * @param p El string que indica la pieza que se creara.
	 * @param c El color que se hara la pieza.
	 * @return La pieza creada.
	 * @throws ExcepcionPiezaDesconocida La excepcion pieza desconocida
	 */
	public static Pieza creaPieza(String p, Color c) throws ExcepcionPiezaDesconocida {
		init();
			Pieza P = null;
			try {
				Class<?> c1 = Class.forName("modelo." + p);	
				//Class<?>[] paramTypes = new Class<?>[] {modelo.Color.class};
				Constructor<?> m = c1.getConstructor(Color.class);
//				Color[] arguments = new Color[] {c};
				P = (Pieza) m.newInstance(c);
				return P;
			} catch (Exception e) {
				throw new ExcepcionPiezaDesconocida(p.charAt(0));
			}
		}
	
	/**
	 * Crea pieza mediante el caracter y el color.
	 *
	 * @param p El caracter de la pieza.
	 * @param c El color que tendra la pieza
	 * @return La pieza creada en el tablero.
	 * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
	 */
	public static Pieza creaPieza(char p, Color c) throws ExcepcionPiezaDesconocida {
		init();
		String s = tablaPiezas.get(p);
			return creaPieza(s,c);
	}
}
