package modelo;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCasillaOrigenVacia;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionTurnoDelContrario;

/**
 * The Interface EvaluadorAjedrez.
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public interface EvaluadorAjedrez {

	/**
	 * Evalua.
	 *
	 * @param pa the pa
	 * @param c the c
	 * @return the double
	 */
	public double evalua(PartidaAjedrez pa, Color c);
	
	/**
	 * Evalua movimiento.
	 *
	 * @param pa the pa
	 * @param c the c
	 * @param n the n
	 * @return the double
	 * @throws ExcepcionNoExisteMovimiento the excepcion no existe movimiento
	 * @throws ExcepcionCasillaOrigenVacia the excepcion casilla origen vacia
	 * @throws ExcepcionTurnoDelContrario the excepcion turno del contrario
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 */
	public double evaluaMovimiento(PartidaAjedrez pa, Color c, int n) throws ExcepcionNoExisteMovimiento, ExcepcionCasillaOrigenVacia, ExcepcionTurnoDelContrario, ExcepcionMovimientoIlegal, ExcepcionCasillaDestinoOcupada;
	
}
