package modelo;

import java.util.HashSet;
import java.util.Set;



/**
 * La clase Alfil. Hereda de Pieza.
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class Alfil extends Pieza {

	/** El tipo de la pieza. */
	private static final char tipo = 'A';
	
	/**
	 * Constructor del Alfil.
	 *
	 * @param c El color que sera el alfil.
	 */
	public Alfil(Color c) {
		super(c);
		this.valor = 3.0;
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#isMismoTipo(modelo.Pieza)
	 */
	@Override
	public boolean isMismoTipo(Pieza p) {
		if(this.getTipo() == p.getTipo()) {
			return true;
		}else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#getCasillasAmenazadas()
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas() {
		Set<Casilla> Lista = new HashSet<Casilla> ();
		if(this.getCasilla() == null) {
			System.err.println("La casilla de la pieza: " + this.toString() + " es nula.");
		}else {
			char Letra = this.getCasilla().getCoordenada().getLetra();
			int Y = this.getCasilla().getCoordenada().getY();
			Lista = getCasillasAmenazadasAlfil(Letra,Y);	
		}
		return Lista;
	}

	/**
	 * Getter de las casillas amenazadas por el alfil.
	 *
	 * @param L La posicion X del Alfil.
	 * @param Y La posicion Y del Alfil.
	 * @return Las casillas que amenaza el Alfil.
	 */
	private Set<Casilla> getCasillasAmenazadasAlfil(char L, int Y) {
			Set<Casilla> Lista = new HashSet<Casilla>();
					Lista.addAll(T.getDiagonalNE(this.getCasilla()));
					Lista.addAll(T.getDiagonalSE(this.getCasilla()));
					Lista.addAll(T.getDiagonalSO(this.getCasilla()));
					Lista.addAll(T.getDiagonalNO(this.getCasilla()));
			return Lista;
		}

	/* (non-Javadoc)
	 * @see modelo.Pieza#getTipo()
	 */
	@Override
	public char getTipo() {
		return tipo;
	}

}
