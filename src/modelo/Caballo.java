package modelo;

import java.util.HashSet;
import java.util.Set;

/**
 * La clase Caballo.
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class Caballo extends Pieza {

	/** El tipo de la pieza. */
	private static final char tipo = 'C';
	
	/**
	 * Constructor de la Pieza Caballo.
	 *
	 * @param c El color de la pieza.
	 */
	public Caballo(Color c) {
		super(c);
		this.valor = 3.0;
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#isMismoTipo(modelo.Pieza)
	 */
	@Override
	public boolean isMismoTipo(Pieza p) {
		if(this.getTipo() == p.getTipo()) {
			return true;
		}else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#getCasillasAmenazadas()
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas() {
		Set<Casilla> Lista = new HashSet<Casilla> ();
			if(this.getCasilla() == null) {
				System.err.println("La casilla de la pieza: " + this.toString() + " es nula.");
			}else {
				Lista.addAll(T.getSaltosCaballo(this.getCasilla()));
			}
		return Lista;
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#getTipo()
	 */
	@Override
	public char getTipo() {
		return tipo;
	}

}
