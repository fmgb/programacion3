package modelo;

import java.util.ArrayList;
import java.util.List;

import modelo.excepciones.ExcepcionCoordenadaErronea;

/**
 * La Clase Tablero.
 * 
 * @author Francisco Manuel García Botella 48570019-E
 */
public class Tablero {

	/** Las casillas. */
	private Casilla[][] casillas;

	/** La dimx. */
	private int dimx;

	/** La dimy. */
	private int dimy;

	/** El max dimx. */
	public final int MAX_DIMX = 26; // Maximo valor de las columnas

	/** El max dimy. */
	public final int MAX_DIMY = Integer.MAX_VALUE; // Maximo valor de las filas.

	/** El numero de casillas xdefecto. */
	public final int kXDEFECTO = 8; // Si alguna no es correcta se le pasa este
	// valor.

	/**
	 * Constructor: Inicializar tablero con los valores pasados. Utiliza elmetodo privado convertiraChar donde le pasamos un numero y lo convierte en char.
	 *
	 * @param dimx Dimension del ancho.
	 * @param dimy Dimension de lo largo.
	 */
	public Tablero(int dimx, int dimy) {
		// Comprobamos X
		if (dimx > 0 && dimx <= MAX_DIMX && dimx % 2 == 0) // Si las columnas >
			// 0 y < 26. Y que
			// sean par.
		{ // Si es valida la medida, la pondremos. Sino ira a por defecto 8.
			this.dimx = dimx;
		} else // Sino ponemos las columans a 8 y comprobamos las filas.
		{
			this.dimx = kXDEFECTO;
		}
		// Comprobamos Y.
		if (dimy > 0 && dimy <= MAX_DIMY && dimy % 2 == 0)// Si las filas son >0
			// y <MAXDIMY. Y que
			// sean par.
		{
			this.dimy = dimy;
		} else // Sino asignamos las filas a 8.
		{
			this.dimy = kXDEFECTO;
		}

		casillas = new Casilla[this.dimx][this.dimy + 1]; 
		// Empiezo abajo a la izquierda. Con la posicion [0][1]. Ignoramos la
		for (int y = this.dimy; y > 0; y--) { // Inicio en this.dimy hasta 1.
			for (int x = 0; x < this.dimx; x++) { // Inicio en 0 y this.dimx-1.
				// Siendo 'A' = 0, 'B' =
				// 1,etc...
				casillas[x][y] = new Casilla(); // Reservamos memoria.
				if (x % 2 == 1) // Columnas Impar.
				{
					if (y % 2 == 0) // Y par. Filas impar en el tablero.
					{ // Asignamos el color y la coordenada.
						casillas[x][y].setColor(Color.NEGRO);
						try {
							casillas[x][y].setCoordenada(ConvertirIntaChar(x),y);
						} catch (ExcepcionCoordenadaErronea e) {
							System.err.println(e.getMessage());
						}
					}
					if (y % 2 == 1) // Y impar. Filas par en el tablero.
					{// Asignamos el color y la coordenada.
						casillas[x][y].setColor(Color.BLANCO);
						try {
							casillas[x][y].setCoordenada(ConvertirIntaChar(x),y);
						} catch (ExcepcionCoordenadaErronea e) {
							System.err.println(e.getMessage());
						}
					}
				}
				if (x % 2 == 0) // Columnas par.
				{
					if (y % 2 == 0) // Y par. Filas impar en el tablero.
					{// Asignamos el color y la coordenada.
						casillas[x][y].setColor(Color.BLANCO);
						try {
							casillas[x][y].setCoordenada(ConvertirIntaChar(x),y);
						} catch (ExcepcionCoordenadaErronea e) {
							System.err.println(e.getMessage());
						}
					}
					if (y % 2 == 1) // Y Impar. Filas par en el tablero
					{// Asignamos el color y la coordenada.
						casillas[x][y].setColor(Color.NEGRO);
						try {
							casillas[x][y].setCoordenada(ConvertirIntaChar(x),
									y);
						} catch (ExcepcionCoordenadaErronea e) {
							System.err.println(e.getMessage());
						}
					}
				}
			}
		}
	}

	/**
	 * Constructor de copia.
	 *
	 * @param t Pasamos el parametro t que es un tablero que queremos copiar.
	 */
	public Tablero(Tablero t) {
		this(t.getDimx(), t.getDimy());
	}

	/**
	 * Getter Dimx.
	 * 
	 * @return Devuelve el valor de la dimension X del tablero.
	 */
	public int getDimx() {
		return dimx;
	}

	/**
	 * Getter Dimy.
	 * 
	 * @return Devuelve el valor de la dimension Y del tablero.
	 */
	public int getDimy() {
		return dimy;
	}

	/**
	 * Getter que devuelve la casilla asociada a los parametros que le pasamos.
	 *
	 * @param c Coordenada que comprobaremos la casilla.
	 * @return Devuelve CasillaError o la referencia a Casilla.
	 * @throws ExcepcionCoordenadaErronea La excepcion coordenada erronea
	 */
	public Casilla getCasillaAt(Coordenada c) throws ExcepcionCoordenadaErronea {
		if (ConvertirCharaInt(c.getLetra()) >= dimx || c.getY() <= 0 || ConvertirCharaInt(c.getLetra()) < 0 || c.getY() > dimy || c == Coordenada.coordenadaError) {
			throw new ExcepcionCoordenadaErronea(c.getLetra(), c.getY());
		} else {
			return casillas[ConvertirCharaInt(c.getLetra())][c.getY()];
		}
	}

	/**
	 * Getter que devuelve la casilla asociada a los parametros que le pasamos.
	 *
	 * @param l Letra de la coordenada de la casilla.
	 * @param y Entero de la coordenada de la casilla.
	 * @return Devuelve o CasillaError o la referencia a Casilla.
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public Casilla getCasillaAt(char l, int y) throws ExcepcionCoordenadaErronea {
		Coordenada c;
		c = new Coordenada(l, y);
		if (c != Coordenada.coordenadaError) {
			return getCasillaAt(c);
		} else {
			throw new ExcepcionCoordenadaErronea(l, y);
		}
	}

	/**
	 * Coloca la pieza que le pasamos en la coordenada indicada.
	 *
	 * @param c La coordenada donde queremos colocar la pieza.
	 * @param p La pieza que queremos colocar.
	 * @return true, si la coloca, false si no la coloca.
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public boolean colocaPiezaAt(Coordenada c, Pieza p)
			throws ExcepcionCoordenadaErronea {
		if (getCasillaAt(c).setPieza(p))
			return true;
		else
			return false;
	}

	/**
	 * To string.
	 *
	 * @return String devuelve las casillas ocupadas del tablero.
	 * 
	 */
	public String toString() {
		String s = "";
		int uno = 1, cero = 0;
		char intro = '\n';

		for (int y = this.dimy; y > 0; y--) {
			for (int x = 0; x < this.dimx; x++) {
				if (casillas[x][y].isOcupada()) {
					s = s + uno;
				} else {
					s = s + cero;
				}
			}
			s = s + intro;
		}
		return s;
	}

	/**
	 * Convertir a Char metodo privado que convierte el entero que le pasemos a Char.
	 * 
	 * @param n El entero que queremos convertir.
	 * @return El char que equivale a ese numero. 'A' es el 0. 'B' el 1, etc.
	 */
	private char ConvertirIntaChar(int n) {
		return ((char) (n + 'A'));
	}

	/**
	 * Convertir Char a Int Método privado que convierte en entero el char que le pasemos.
	 * 
	 * @param l El Char que queremos convertir.
	 * @return El entero que equivale al char que le hemos pasado.
	 */
	private int ConvertirCharaInt(char l) { 
		return ((int) (l - 'A'));
	}

	/**
	 * Getter the columna arriba.
	 *
	 * @param c La casilla en la que se parte hacia arriba.
	 * @return La lista de casillas que estan arriba.
	 */
	public List<Casilla> getColumnaArriba (Casilla c) { //Arriba Y++ && X =
		List<Casilla> ListaC = new ArrayList <Casilla>();
		if(c == null) {
			System.out.println("La casilla es nula");
		}else {
			int i = c.getCoordenada().getY();
			char Letra = c.getCoordenada().getLetra();
			//		System.out.println("HOLA");
			if(casillaEstaEnTablero(Letra, i)) {

				for(int Y = i + 1; Y <= this.getDimy(); Y++) {
					try {
						ListaC.add(getCasillaAt(Letra,Y));
						//				System.out.println("HAYPIEZA " +getCasillaAt(Letra, Y).isOcupada());
						if(getCasillaAt(Letra, Y).isOcupada()) {
							Y = this.getDimy() + 1; //Para salir del bucle
						}
					}catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage() + " Tablero");
					}
				}//FIN FOR
			}//FIN IF ESTA TABLERO
			else {
				try {
					throw new ExcepcionCoordenadaErronea(Letra, i);
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage()+" Tablero");
				}
			}
		}//ELSE ES NULA
		return ListaC;
	}// FIN GETFILADERECHA

	/**
	 * Getter La columna Abajo.
	 *
	 * @param c La casilla en la que se parte hacia abajo.
	 * @return La lista de casillas que estan abajo.
	 */
	public List<Casilla> getColumnaAbajo(Casilla c) { // Abajo Y-- && X=
		List<Casilla> ListaC = new ArrayList <Casilla>();
		if(c == null) {
			System.out.println("La casilla es nula");
		}else {
			int i = c.getCoordenada().getY();
			int Y ;
			char Letra = c.getCoordenada().getLetra();
			Y = i-1;
			//	System.out.println("LEtra" + Letra + " Numero " + Y);
			if(casillaEstaEnTablero(Letra, i)) {
				for( Y= i - 1; Y > 0; Y--) {
					try{
						ListaC.add(getCasillaAt(Letra,Y));

						if(PartidaAjedrez.getInstancia().getTablero().getCasillaAt(Letra,Y).isOcupada()) {

							Y = 0;
						}
					}catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage() + " Tablero4");
					}
				}//FIN FOR*/
			}			
			else {
				try {
					throw new ExcepcionCoordenadaErronea(Letra, i);
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage()+" Tablero3");
				}
			}
		}
		return ListaC;
	}//FIN GETFILAIZQUIERDA

	/**
	 * Gets the fila derecha.
	 *
	 * @param c the c
	 * @return the fila derecha
	 */
	public List<Casilla> getFilaDerecha(Casilla c) { // Derecha X++ && Y =
		List<Casilla> ListaC = new ArrayList <Casilla> ();
		if(c == null) {
			System.out.println("La casilla es nula");
		}else {
			int Y = c.getCoordenada().getY();
			char Letra = c.getCoordenada().getLetra();
			if(casillaEstaEnTablero(Letra, Y)) {
				int L = ConvertirCharaInt(Letra);
				for(int X = L + 1; X < this.getDimy(); X++) {
					try {
						ListaC.add(getCasillaAt(ConvertirIntaChar(X),Y));
						if(getCasillaAt(ConvertirIntaChar(X), Y).isOcupada()) {
							X = this.getDimy() + 1; //Para salir del bucle
						}
					}catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage() + "Tablero5");
					}
				}//FIN FOR
			}//FIN IF CASILLAESTA 			
			else {
				try {
					throw new ExcepcionCoordenadaErronea(Letra, Y);
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage()+ " Tablero6");
				}
			}
		}//ELSE ES NULA
		return ListaC;
	}//FIN GETCOLUMNAARRIBA

	/**
	 * Gets the fila izquierda.
	 *
	 * @param c the c
	 * @return the fila izquierda
	 */
	public List<Casilla> getFilaIzquierda(Casilla c) { // Izquierda; X-- && Y=
		List<Casilla> ListaC = new ArrayList <Casilla> ();
		if(c == null) {
			System.out.println("La casilla es nula");
		}else {
			int Y = c.getCoordenada().getY();
			char Letra = c.getCoordenada().getLetra();
			if(casillaEstaEnTablero(Letra, Y)) {
				int L = ConvertirCharaInt(Letra);
				for(int X = L - 1; X >= 0; X--) {
					try {
						ListaC.add(getCasillaAt(ConvertirIntaChar(X),Y));
						if(getCasillaAt(ConvertirIntaChar(X), Y).isOcupada()) {
							X = -1; //Para salir del bucle
						}
					}catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage()+ " Tablero7");
					}
				}//FIN FOR
			}//FIN IF DENTROTABLERO
			else {
				try {
					throw new ExcepcionCoordenadaErronea(Letra, Y);
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage()+ " Tablero8");
				}
			}
		}//ELSE ES NULA
		return ListaC;
	}//FIN GetCcolumnaAbajo

	/**
	 * Gets the diagonal no.
	 *
	 * @param c the c
	 * @return the diagonal no
	 */
	public List<Casilla> getDiagonalNO(Casilla c) { // Arriba Izquierda.  Y++ && X--
		List <Casilla> ListaC = new ArrayList <Casilla> ();
		if(c == null) {
			System.out.println("La casilla es nula");
		}else {
			int i = c.getCoordenada().getY();
			char Letra = c.getCoordenada().getLetra();
			if(casillaEstaEnTablero(Letra,i)) {
				int L = ConvertirCharaInt(Letra);
				int X = L - 1;	
				for(int Y = i + 1; Y <= this.getDimy() && X >= 0; Y++) {

					try {
						ListaC.add(getCasillaAt(ConvertirIntaChar(X),Y));
						if(getCasillaAt(ConvertirIntaChar(X),Y).isOcupada() || X == 0) { // CHECK 
							Y = this.getDimy() +1;
						}
						X--;
					} catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage()+ " Tablero9");
					}
				}//FIN FOR i
			}
			else {
				try {
					throw new ExcepcionCoordenadaErronea(Letra, i);
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage()+" Tablero10");
				}
			}
		}
		return ListaC;
	}

	/**
	 * Gets the diagonal ne.
	 *
	 * @param c the c
	 * @return the diagonal ne
	 */
	public List<Casilla> getDiagonalNE(Casilla c) { //Arriba Derecha. Y++ && X++
		List <Casilla> ListaC = new ArrayList <Casilla> ();
		if(c == null) {
			System.out.println("La casilla es nula");
		}else {
			int i = c.getCoordenada().getY();
			char Letra = c.getCoordenada().getLetra();
			if(casillaEstaEnTablero(Letra,i)) {
				int L = ConvertirCharaInt(Letra);
				int X = L + 1;	
				for(int Y = i + 1; Y <= this.getDimy() && X < this.getDimx(); Y++) {

					try {
						ListaC.add(getCasillaAt(ConvertirIntaChar(X),Y));
						if(getCasillaAt(ConvertirIntaChar(X),Y).isOcupada() || X == this.getDimx()) { // CHECK 
							Y = this.getDimy() +1;
						}
						X++;
					} catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage()+ " Tablero11");
					}
				}//FIN FOR i
			}else {
				try {
					throw new ExcepcionCoordenadaErronea(Letra, i);
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage()+ " Tablero12");
				}
			}
		}
		return ListaC;
	}

	/**
	 * Gets the diagonal so.
	 *
	 * @param c the c
	 * @return the diagonal so
	 */
	public List<Casilla> getDiagonalSO(Casilla c) { // Y--  && X--;Abajo izquierda
		List <Casilla> ListaC = new ArrayList <Casilla> ();
		if(c == null) {
			System.out.println("La casilla es nula");
		}else {
			int i = c.getCoordenada().getY();
			char Letra = c.getCoordenada().getLetra();
			if(casillaEstaEnTablero(Letra,i)) {
				int L = ConvertirCharaInt(Letra);
				int X = L - 1;	
				for(int Y = i - 1; Y > 0 && X >= 0; Y--) {

					try {
						ListaC.add(getCasillaAt(ConvertirIntaChar(X),Y));
						if(getCasillaAt(ConvertirIntaChar(X),Y).isOcupada() || X == 0) { // CHECK 
							Y = - 1;
						}
						X--;
					} catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage()+ " Tablero2");
					}
				}//FIN FOR i
			}
			else {
				try {
					throw new ExcepcionCoordenadaErronea(Letra, i);
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage()+ "Tablero1");
				}
			}
		}
		return ListaC;
	}

	/**
	 * Gets the diagonal se.
	 *
	 * @param c the c
	 * @return the diagonal se
	 */
	public List<Casilla> getDiagonalSE(Casilla c) { // Y--  && X++;Abajo derecha
		List <Casilla> ListaC = new ArrayList <Casilla> ();
		if(c == null) {
			System.out.println("La casilla es nula");
		}else {
			int Y = c.getCoordenada().getY();
			char Letra = c.getCoordenada().getLetra();
			if(casillaEstaEnTablero(Letra,Y)) {
				int L = ConvertirCharaInt(Letra);
				int X = L + 1;	
				for(int i = Y - 1; i > 0 && X < this.getDimx(); i--) {

					try {
						ListaC.add(getCasillaAt(ConvertirIntaChar(X),i));
						if(getCasillaAt(ConvertirIntaChar(X),i).isOcupada() || X == this.getDimx()) { 
							i = - 1;
						}
						X++;
					} catch (ExcepcionCoordenadaErronea e) {
						System.err.println(e.getMessage());
					}
				}//FIN FOR i
			}
			else {
				try {
					throw new ExcepcionCoordenadaErronea(Letra, Y);
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage());
				}
			}
		}
		return ListaC;
	}

	/**
	 * Gets the saltos caballo.
	 *
	 * @param c the c
	 * @return the saltos caballo
	 */
	public List<Casilla> getSaltosCaballo(Casilla c) { //COMPROBADO
		List<Casilla> Lista = new ArrayList<Casilla> ();

		if(c == null) {
			return Lista;
		} 
		else
		{
			int Y = c.getCoordenada().getY();
			char L = c.getCoordenada().getLetra();
			try {
				if(casillaEstaEnTablero(L, Y)){
					if(casillaEstaEnTablero((char) (L + 1), Y + 2)){ //1
						Lista.add(this.getCasillaAt((char) (L + 1), Y + 2));
					}
					if(casillaEstaEnTablero((char) (L + 2), Y + 1)){ //2
						Lista.add(this.getCasillaAt((char) (L+2), Y+1));
					}
					if(casillaEstaEnTablero((char) (L + 2), Y - 1)){ //3
						Lista.add(this.getCasillaAt((char) (L+2), Y -1));
					}
					if(casillaEstaEnTablero((char) (L + 1), Y - 2)){//4
						Lista.add(this.getCasillaAt((char) (L + 1), Y - 2));
					}
					if(casillaEstaEnTablero((char) (L - 1), Y - 2)){//5
						Lista.add(this.getCasillaAt((char) (L - 1), Y - 2));
					}
					if(casillaEstaEnTablero((char) (L - 2), Y - 1)){ //6
						Lista.add(this.getCasillaAt((char) (L - 2), Y -1));
					}
					if(casillaEstaEnTablero((char) (L - 2), Y + 1)){//7
						Lista.add(this.getCasillaAt((char) (L - 2), Y + 1));
					}
					if(casillaEstaEnTablero((char) (L - 1), Y + 2)){//8
						Lista.add(this.getCasillaAt((char) (L - 1), Y + 2));
					}

				}
			} catch (ExcepcionCoordenadaErronea e) {
				System.err.println(e.getMessage());
			}
		}
		return Lista;
	}

	/**
	 * Casilla esta en tablero.
	 * @param Letra the letra
	 * @param Y the y
	 * @return true, if successful
	 */
	public boolean casillaEstaEnTablero(char Letra, int Y) {
		boolean esta = false;
		if(Letra >= 'A' && Letra < ConvertirIntaChar(this.getDimy()) && Y > 0 && Y<=this.getDimy()) {
			esta = true;
		}
		return esta;
	}

	/**
	 * Gets the amenazas.
	 *
	 * @param cas the cas
	 * @param col the col
	 * @return the amenazas
	 */
	public List<Pieza> getAmenazas(Casilla cas, Color col) {
		List<Pieza> Lista = new ArrayList<Pieza>();
		List<Pieza> ListaAux = new ArrayList<Pieza>();
		PartidaAjedrez PA = PartidaAjedrez.getInstancia();
		if(cas == null) {
			System.err.println("La casilla: es nula.");
		} 
		else
		{
			ListaAux = PA.getPiezas(col);
			for(int i = 0; i < ListaAux.size();i++){
				if(ListaAux.get(i).getCasilla() == null) {
					
				} else if(ListaAux.get(i).getCasillasAmenazadas().contains(cas)) {
					Lista.add(ListaAux.get(i));
				}
			}
		}
		return Lista;
	}
} // FIN CLASE TABLERO
