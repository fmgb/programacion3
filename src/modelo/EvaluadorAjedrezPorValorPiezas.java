package modelo;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCasillaOrigenVacia;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionTurnoDelContrario;

/**
 * The Class EvaluadorAjedrezPorValorPiezas.
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class EvaluadorAjedrezPorValorPiezas implements EvaluadorAjedrez {


	/* (non-Javadoc)
	 * @see modelo.EvaluadorAjedrez#evalua(modelo.PartidaAjedrez, modelo.Color)
	 */
	@Override
	public double evalua(PartidaAjedrez pa, Color c) { //CORRECTO!
		double Resultado = 0.0;
		int X = pa.getTablero().getDimx();
		int dimY = pa.getTablero().getDimy();

		char dimX = ((char) (X + 'A'));

		for(char i = 'A'; i < dimX;i++) {
			for(int j = 1; j <= dimY; j++) {
				try {
					if(pa.getTablero().getCasillaAt(i, j).isOcupada()) {

						if(pa.getTablero().getCasillaAt(i,j).getPieza().getColor() == c) {

							Resultado = Resultado + pa.getTablero().getCasillaAt(i, j).getPieza().getValor();
						}
					}
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage());
				}
			}
		}
		return Resultado;
	}

	/* (non-Javadoc)
	 * @see modelo.EvaluadorAjedrez#evaluaMovimiento(modelo.PartidaAjedrez, modelo.Color, int)
	 */
	@Override
	public double evaluaMovimiento(PartidaAjedrez pa, Color c, int n) throws ExcepcionNoExisteMovimiento, ExcepcionCasillaOrigenVacia, ExcepcionTurnoDelContrario, ExcepcionMovimientoIlegal, ExcepcionCasillaDestinoOcupada {
		pa.ejecutaMovimiento(n);
        return this.evalua(pa, c);
	}

}
