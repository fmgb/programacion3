package modelo;

import java.util.HashSet;
import java.util.Set;


/**
 * La clase Dama.
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class Dama extends Pieza {
	
	/** El tipo de la pieza. */
	private static final char tipo = 'D';
	
	/**
	 * Constructor de Dama.
	 *
	 * @param c El color de la pieza.
	 */
	public Dama(Color c) {
		super(c);
		this.valor = 9.0;
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#isMismoTipo(modelo.Pieza)
	 */
	@Override
	public boolean isMismoTipo(Pieza p) {
		if(this.getTipo() == p.getTipo()) {
			return true;
		}else {
			return false;
		}
	}


	/* (non-Javadoc)
	 * @see modelo.Pieza#getCasillasAmenazadas()
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas() {
		Set<Casilla> Lista = new HashSet<Casilla> ();
		if(this.getCasilla() == null) {
			System.err.println("La casilla de la pieza: " + this.toString() + " es nula.");
		}else {
			char Letra = this.getCasilla().getCoordenada().getLetra();
			int Y = this.getCasilla().getCoordenada().getY();
			Lista = getCasillasAmenazadasDama(Letra,Y);	
		}
		return Lista;
	}

	/**
	 * Getter de las casillas que amenaza la Dama.
	 *
	 * @param letra La letra de la Coordenada de la Dama.
	 * @param y El numero de la coordenada de la Dama.
	 * @return Las casillas que amenaza la Dama.
	 */
	private Set<Casilla> getCasillasAmenazadasDama(char letra, int y) {
		Set<Casilla> Lista = new HashSet<Casilla>();
				Lista.addAll(T.getColumnaAbajo(this.getCasilla()));
				Lista.addAll(T.getColumnaArriba(this.getCasilla()));
				Lista.addAll(T.getFilaDerecha(this.getCasilla()));
				Lista.addAll(T.getFilaIzquierda(this.getCasilla()));
				Lista.addAll(T.getDiagonalNE(this.getCasilla()));
				Lista.addAll(T.getDiagonalSE(this.getCasilla()));
				Lista.addAll(T.getDiagonalSO(this.getCasilla()));
				Lista.addAll(T.getDiagonalNO(this.getCasilla()));
		return Lista;
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#getTipo()
	 */
	@Override
	public char getTipo() {
		return tipo;
	}

}
