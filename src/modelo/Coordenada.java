package modelo;

import modelo.excepciones.ExcepcionCoordenadaErronea;

/**
 * La Clase Coordenada.
 * 
 * @author Francisco Manuel Garcia Botella 48570019-E
 */

public class Coordenada {

	/** La constante Coordenada coordenadaError. */
	public static final Coordenada coordenadaError = new Coordenada();

	/** La letra de la coordenada. */
	private char letra; // La letra del tablero

	/** La y de la coordenada. */
	private int y; // El numero del tablero
	
	/**
	 * Constructor por defecto de coordenada.
	 */
	public Coordenada() { // Constructor por defecto de Coordenada
		letra = '0';
		y = 0;
	}

	/**
	 * Constructor de Coordenada. Le añade los dos parametros a la nueva coordenada.
	 *
	 * @param le Es la letra de la posicion de la coordenada.
	 * @param y Es el numero de la posicion de la coordenada.
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public Coordenada(char le, int y) throws ExcepcionCoordenadaErronea { // Constructor	 parametros
		if (le < 'A' || le > 'Z' || y < 0) {
			throw new ExcepcionCoordenadaErronea(le, y);
		}
		letra = le;
		this.y = y;

	}

	/**
	 * Constructor de Copia de Coordenada.
	 * 
	 * @param otra Le pasamos la coordenada que queremos copiar.
	 */
	public Coordenada(Coordenada otra) // Constructor de copia
	{
		this.letra = otra.letra;
		this.y = otra.y;
	}

	/**
	 * Getter X de Coordenada.
	 * 
	 * @return Devolvemos la letra de una coordenada.
	 */
	public char getLetra() { // Metodo que nos devuelve la letra.
		return letra;
	}

	/**
	 * Getter Y de Coordenada.
	 * 
	 * @return Devuelve el valor Y de una coordenada.
	 */
	public int getY() { // Metodo que nos devuelve el numero.
		return y;
	}

	/**
	 * Metodo que nos permite imprimir la coordenada.
	 */
	public void imprimir() { // metodo que imprime por pantalla la coordenada.
		System.out.print(letra);
		System.out.print(y);
	}

	/**
	 * Metodo que verifica si el objeto que le pasamos tiene los mismos
	 * parametros que la coordenada.
	 * 
	 * @param obj El objeto que le pasamos para comprobar si es igual.
	 * @return true si las coordenadas son iguales, sino false.
	 */
	public boolean equals(Object obj) { // Metodo que verifica si una coordenada
										// tiene los mismos parametros que otra
										// coordenada
		if (obj == this)
			return true;

		if (obj == null)
			return false;

		if (!(obj instanceof Coordenada))
			return false;

		Coordenada otro = (Coordenada) obj;

		if (otro.letra != letra)
			return false;

		if (otro.y != y)
			return false;

		return true;
	}

	/**
	 * Metodo que une la letra y el numero de la coordenada.
	 * 
	 * @return String La String esta compuesta por la letra y el numero de la coordenada.
	 */
	public String toString() { // Metodo que une la letra y el numero de la
								// coordenada
		StringBuilder s = new StringBuilder();
		s.append(letra);
		s.append(y);
		return s.toString();

	}
}
