package modelo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCasillaOrigenVacia;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimientoCoronacion;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;
import modelo.excepciones.ExcepcionTurnoDelContrario;


/**
 * La Clase PartidaAjedrez.
 * @author Francisco Manuel García Botella 48570019-E
 */
public class PartidaAjedrez {

	/** La dimensión del tablero. */
	public final int kDIMT = 8;

	/** La instancion. */
	private static PartidaAjedrez instancia;

	/** String del fichero entrada. */
	private String fentrada;

	/** String del fichero salida. */
	private String fsalida;

	/** El turno de la partida. */
	private Color turno = Color.BLANCO;

	/** El tablero de la partida. */
	private Tablero T;

	/** Los movimientos de la partida. */
	private ArrayList<Movimiento> movs = new ArrayList<Movimiento>();

	/** Las piezas en el tablero de la partida. */
	public ArrayList<Pieza> piezas = new ArrayList<Pieza>(); //CHANGE A privado.

	/** String auxiliar. */
	private ArrayList<String> strings = new ArrayList<String>();

	/** El Scanner. */
	private Scanner in;

	/**
	 * Constructor del Tablero de la partida.
	 */
	protected PartidaAjedrez() { T = new Tablero(kDIMT, kDIMT); }

	/**
	 * Getter.
	 *
	 * @return La instancia del PartidaAjedrez.
	 */
	public static PartidaAjedrez getInstancia() { 
		if (instancia != null) { // Si ya ha sido creada instancia, devolvemos instancia.
			return instancia;
		} else { // Sino, creamos una nueva instancia y la devolvemos.
			instancia = new PartidaAjedrez();
			return instancia;
		}
	}

	/**
	 * Setter del fichero de entrada.
	 *
	 * @param f El nombre del fichero
	 */
	public void setEntrada(String f) { fentrada = f; }

	/**
	 * Setter de la salida de fichero.
	 *
	 * @param f El nombre del fichero de salida.
	 */
	public void setSalida(String f) { fsalida = f; }

	/**
	 * Getter de la entrada de fichero.
	 *
	 * @return El nombre del fichero de entrada.
	 */
	public String getEntrada() { return fentrada; }

	/**
	 * Getter de salida.
	 *
	 * @return el nombre del fichero de salida.
	 */
	public String getSalida() { return fsalida; }

	/**
	 * Getter del tablero.
	 *
	 * @return El tablero.
	 */
	public Tablero getTablero() { return T; }

	/**
	 * Getter de la pieza del tablero.
	 *
	 * @param c La coordenada
	 * @return La pieza
	 * @throws ExcepcionCoordenadaErronea Excepcion de coordenada erronea.
	 */
	public Pieza getPiezaAt(Coordenada c) throws ExcepcionCoordenadaErronea { 
		if (convertirCharaInt(c.getLetra()) > T.getDimx()|| c.getY() > T.getDimy()) {
			throw new ExcepcionCoordenadaErronea(c.getLetra(), c.getY());
		}
		if (!T.getCasillaAt(c).isOcupada()) { // SI no está ocupada devuelve null
			return null;
		} else { // Sino la pieza.
			return T.getCasillaAt(c).getPieza();
		}
	}

	/**
	 * Getter el movimiento at.
	 *
	 * @param i el numero del movimiento
	 * @return El movimiento
	 * @throws ExcepcionNoExisteMovimiento the excepcion no existe movimiento
	 */
	public Movimiento getMovimientoAt(int i) throws ExcepcionNoExisteMovimiento { 
		if (i >= movs.size()) {
			throw new ExcepcionNoExisteMovimiento(i);
		} else {
			return movs.get(i);
		}
	}

	/**
	 * Añade un movimiento.
	 *
	 * @param co La coordenada origen.
	 * @param cd La coordenada destino.
	 * @throws ExcepcionCoordenadaErronea La excepcion coordenada erronea
	 */
	public void addMovimiento(Coordenada co, Coordenada cd) throws ExcepcionCoordenadaErronea { 
		if (convertirCharaInt(co.getLetra()) > T.getDimx() || co.getY() > T.getDimy()) { // Si no son del tablero las coordenadas salta Excepcion.
			throw new ExcepcionCoordenadaErronea(co.getLetra(), co.getY());
		}
		if (co.getLetra() == cd.getLetra() && co.getY() == cd.getY()) {
			throw new ExcepcionCoordenadaErronea(co.getLetra(), co.getY());
		}
		movs.add(new MovimientoOrdinario(co, cd)); // Añadimos movimiento al array
	}

	/**
	 * toString.
	 *
	 * @return Devuelve la String del tablero.
	 */
	public String toString() {
		String s = "";
		char intro = '\n';
		String guion = "- ";
		for (int y = getTablero().getDimy(); y > 0; y--) {
			for (int x = 0; x < getTablero().getDimx(); x++) {
				try {
					 Coordenada c = new Coordenada(convertirIntaChar(x), y);
	                    if (this.getPiezaAt(c) == null) {
	                        s = s + guion;
	                    } else {
	                        if (this.getPiezaAt(c).getColor() == Color.BLANCO) {
	                            s = s + this.getPiezaAt(c).getTipo() + " ";
	                        } else {
	                            s = s + Character.toLowerCase(this.getPiezaAt(c).getTipo()) + " ";
	                        }
	                    }
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage() + "1 PartidaAjedrez");
				}
			}
			if(y != 1) {
				s = s + intro;
			}
		}
		return s;
	}

	/**
	 * Run.
	 *
	 * @throws FileNotFoundException El archivo no ha sido encontrado.
	 */
	public void run() throws FileNotFoundException { 
		try {
			in = new Scanner(new FileReader(this.getEntrada()));
			this.colocaPiezas();
			this.cargaMovimientos();
		}  finally {
			if (in != null) {
				in.close();
			}
		}
		
		PrintWriter out = null;
		try {
			out = new PrintWriter(fsalida);
			out.print(toString());
		} finally {
			if(out != null)
				out.close();
		}
	} // Fin run
 
	/**
	 * Guarda movimiento.
	 *
	 * @param s La string de la lectura
	 * @return true si ha guardado el movimiento, sino false.
	 */
	private boolean guardaMovimiento(String s){ //Guardamos la string que cogemos del fichero para meterlo en el array de movimientos.
		Coordenada co, cd;
		boolean convertido = false;
		String aux= "", aux2 = "",aux3 = "", aux_aux="";
		aux = s.substring(0, s.indexOf(" ")); // Lectura de la coordenada Origen.
		aux2 = s.substring(s.indexOf(" ") + 1, s.length()); //Lectura desde el 'Espacio' hasta el final de la linea.
		if(aux2.indexOf(" ") != -1) { // Si hay un espacio en blanco es porque hay otra columna.
			aux_aux = aux2.substring(0, s.indexOf(" ")); // Como hay otra columna, obtenemos la coordenada destino.
			aux3 = aux2.substring(s.indexOf(" ") +1, aux2.length()); //La tercera columna que es la de las Piezas. Por lo que aux3 sera la pieza que leemos.
		}
		try {
			if(Character.isDigit(aux.charAt(1)) && Character.isDigit(aux2.charAt(1)) && aux3.isEmpty()) { // Comprobamos las coordenadas Origen y Destino sean del formato AN, Siento A un caracter y N los que quieras. Y que la pieza sea nula(Para evitar NullPointerException).
				co = new Coordenada(aux.charAt(0), convertirCharNumaInt(aux.substring(1, aux.length()))); //Coordenada Origen.
				cd = new Coordenada(aux2.charAt(0), convertirCharNumaInt(aux2.substring(1, aux2.length()))); // Coordenada Destino.
				addMovimiento(co,cd); // Añado movimeintoOrdinario.
				convertido = true;
			}else if(Character.isDigit(aux.charAt(1)) && Character.isDigit(aux2.charAt(1)) && !aux3.isEmpty()) { //Compruebo el formato correcto de las coordenadas Origen y Destino. Y que la HAYA una pieza.
				co = new Coordenada(aux.charAt(0), convertirCharNumaInt(aux.substring(1, aux.length())));
				cd = new Coordenada(aux2.charAt(0), convertirCharNumaInt(aux2.substring(1, aux2.indexOf(" "))));
				Pieza pieza;
				pieza = FactoriaPieza.creaPieza(aux3, Color.NULO); //COLOR NULo porque no sabemos la coordenada que será la pieza.
				addMovimientoCoronacion(co, cd, pieza); // AÑADO EL MOVIMIENTO A MOVIMIENTOCORONACION(metodo Privado).
			}
			else {
				System.err.println("El movimiento no es válido"); //Si no es formato LETRADIGITO
				convertido = false;
			}
		} catch (ExcepcionCoordenadaErronea e) {
			System.err.println(e.getMessage()+"2 PartidaAjedrez");
		}catch (ExcepcionPiezaDesconocida e) {
			System.err.println(e.getMessage());
		}
	//	System.out.println("MOVIMIENTO" + movs.toString());
		return convertido;
	}

	/**
	 * Adds the movimiento coronacion.
	 *
	 * @param co the co
	 * @param cd the cd
	 * @param pieza the pieza
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	private void addMovimientoCoronacion(Coordenada co, Coordenada cd,Pieza pieza) throws ExcepcionCoordenadaErronea {
		if (convertirCharaInt(co.getLetra()) > T.getDimx() || co.getY() > T.getDimy()) { // Si no son del tablero las coordenadas salta Excepcion.
			throw new ExcepcionCoordenadaErronea(co.getLetra(), co.getY());
		}
		if (co.getLetra() == cd.getLetra() && co.getY() == cd.getY()) {
			throw new ExcepcionCoordenadaErronea(co.getLetra(), co.getY());
		}
		movs.add(new MovimientoCoronacion(co, cd,pieza));
		
	}

	/**
	 * Carga movimientos.
	 */
	protected void cargaMovimientos() {
		String columna1 = "";
		String columna2 = "";
		String columnaaux = "";
		String columna3 = "";
		String linea = " ";
		String columnafinal = "";
		
		while (in.hasNext()) { //While Movimientos
			linea = in.nextLine();

			columna1 = linea.substring(0, linea.indexOf(" "));
			columna2 = linea.substring(linea.indexOf(" ") + 1, linea.length());
			if(columna2.indexOf(" ") != -1) {
				columnaaux = columna2.substring(0, columna2.indexOf(" "));
				columna3 = columna2.substring(columna2.indexOf(" ") + 1, columna2.length());
				if(columna3.indexOf(" ") != -1) {
					columna3 = columna3.substring(0,columna3.indexOf(" "));
				}
				columnafinal = columna1 + " " + columnaaux + " " + columna3;
			}else {
				columnafinal = columna1 + " " + columna2;
			}
			System.err.println(columnafinal);
		guardaMovimiento(columnafinal);
		}//Fin While Movimientos
		
		for(int i = 0; i < movs.size(); i++) {
			try {
				ejecutaMovimiento(i);
				
			} catch (ExcepcionNoExisteMovimiento e) {
				System.err.println(e.getMessage()+" 3 PartidaAjedrez");
			} catch (ExcepcionCasillaOrigenVacia e) {
				System.err.println(e.getMessage()+ " 4 PartidaAjedrez");
			} catch (ExcepcionTurnoDelContrario e) {
				System.err.println(e.getMessage()+ " 5 PartidaAjedrez");
			}catch (ExcepcionMovimientoIlegal e) {
				System.err.println(e.getMessage()+ " 6 PartidaAjedrez");
			} catch (ExcepcionCasillaDestinoOcupada e) {
				System.err.println(e.getMessage()+ " 7 PartidaAjedrez");
			} 
		}

	}

	/**
	 * Ejecutamovimiento.
	 *
	 * @param i El numero del movimiento
	 * @throws ExcepcionNoExisteMovimiento la excepcion no existe movimiento
	 * @throws ExcepcionCasillaOrigenVacia la excepcion casilla origen vacia
	 * @throws ExcepcionTurnoDelContrario la excepcion turno del contrario
	 * @throws ExcepcionMovimientoIlegal la excepcion movimiento ilegal
	 * @throws ExcepcionCasillaDestinoOcupada la excepcion casilla destino ocupada
	 */
	protected void ejecutaMovimiento(int i) throws ExcepcionNoExisteMovimiento, ExcepcionCasillaOrigenVacia, ExcepcionTurnoDelContrario, ExcepcionMovimientoIlegal, ExcepcionCasillaDestinoOcupada {
		if(getNumMovimientos() -1 < i) { // Si es mayor el índice, salta excepcion.
			throw new ExcepcionNoExisteMovimiento(i);
		}
		try {
			if(!T.getCasillaAt(movs.get(i).getCoordenadaOrigen()).isOcupada()) {
				throw new ExcepcionCasillaOrigenVacia(movs.get(i));
			}
			if(T.getCasillaAt(movs.get(i).getCoordenadaOrigen()).getPieza().getColor() != turno) {
					throw new ExcepcionTurnoDelContrario(movs.get(i));
			}
			T.getCasillaAt(movs.get(i).getCoordenadaOrigen()).getPieza().mueve(T.getCasillaAt(movs.get(i).getCoordenadaDestino()));
			if(movs.get(i).getClass() == MovimientoCoronacion.class) {
				MovimientoCoronacion MC = (MovimientoCoronacion) movs.get(i);
				Pieza pieza = MC.getPieza();
				pieza.setColor(T.getCasillaAt(movs.get(i).getCoordenadaDestino()).getPieza().getColor());
				try {
					this.coronar((Peon) T.getCasillaAt(movs.get(i).getCoordenadaDestino()).getPieza(),MC.getPieza());
				} catch (ExcepcionMovimientoCoronacion e) {
					System.err.println(e.getMessage());
				}
			}
		} catch (ExcepcionCoordenadaErronea e) {
			System.err.println(e.getMessage()+" 8 PartidaAjedrez");
		}
		if(turno == Color.BLANCO) {
			turno = Color.NEGRO;
		}
		else{
			turno = Color.BLANCO;
		}
	}

	/**
	 * Inicializa tablero.
	 */
	public void inicializaTablero() {
		try {
			 for (char a = 'A'; a < convertirIntaChar(T.getDimx()); a++) {
					Coordenada cpb = new Coordenada(a, 2);
					T.colocaPiezaAt(cpb, FactoriaPieza.creaPieza('P', Color.BLANCO));
					piezas.add(this.getPiezaAt(cpb));
					Coordenada cpn = new Coordenada(a, 7);
					T.colocaPiezaAt(cpn, FactoriaPieza.creaPieza('P', Color.NEGRO));
					piezas.add(this.getPiezaAt(cpn));
					
	                Coordenada c1 = new Coordenada(a, 1);
	                Coordenada c2 = new Coordenada(a, 8);
	                if (a == 'A' || a == 'H') {
	                    T.colocaPiezaAt(c1, FactoriaPieza.creaPieza('T', Color.BLANCO));
	                    piezas.add(this.getPiezaAt(c1));
	                    T.colocaPiezaAt(c2, FactoriaPieza.creaPieza('T', Color.NEGRO));
	                    piezas.add(this.getPiezaAt(c2));

	                } else if (a == 'E') {
	                    T.colocaPiezaAt(c1, FactoriaPieza.creaPieza('R', Color.BLANCO));
	                    piezas.add(this.getPiezaAt(c1));
	                    T.colocaPiezaAt(c2, FactoriaPieza.creaPieza('R', Color.NEGRO));
	                    piezas.add(this.getPiezaAt(c2));
			 		}else if (a == 'C' || a == 'F') {
	                    T.colocaPiezaAt(c1, FactoriaPieza.creaPieza('A', Color.BLANCO));
	                    piezas.add(this.getPiezaAt(c1));
	                    T.colocaPiezaAt(c2, FactoriaPieza.creaPieza('A', Color.NEGRO));
	                    piezas.add(this.getPiezaAt(c2));
	                } else if (a == 'D') {
	                    T.colocaPiezaAt(c1, FactoriaPieza.creaPieza('D', Color.BLANCO));
	                    piezas.add(this.getPiezaAt(c1));
	                    T.colocaPiezaAt(c2, FactoriaPieza.creaPieza('D', Color.NEGRO));
	                    piezas.add(this.getPiezaAt(c2));
	                }  else if (a == 'B' || a == 'G') {
			 			T.colocaPiezaAt(c1, FactoriaPieza.creaPieza('C', Color.BLANCO));
			 			piezas.add(this.getPiezaAt(c1));
                 		T.colocaPiezaAt(c2, FactoriaPieza.creaPieza('C', Color.NEGRO));
                 		piezas.add(this.getPiezaAt(c2));
			 		}
			 }
		} catch (ExcepcionCoordenadaErronea e) {
			System.err.println(e.getMessage()+ " 9 PartidaAjedrez");
		} catch (ExcepcionPiezaDesconocida e) {
			System.err.println(e.getMessage()+ " 10 PartidaAjedrez");
		}
	}

	/**
	 * Coloca piezas.
	 */
	protected void colocaPiezas() { 
		String columna1 = "", columna2 = "", linea = " ", columnafinal ="";
		boolean nohaypiezas = true;
		while (in.hasNext() && linea.length() > 0) { //While Piezas.
			linea = in.nextLine();

			if(linea.length() != 0){//if longitud
				columna1 = linea.substring(0, linea.indexOf(" "));
				columna2 = linea.substring(linea.indexOf(" ") + 1, linea.length());

				if(columna2.indexOf(" ") != -1) {
					columna2 = columna2.substring(0, columna2.indexOf(" "));
				}
				
				columnafinal = columna1 + " " + columna2;
				
				try {
					if(this.isPiezaEnFicherosCorrecta(columnafinal)) {
						Pieza p = crearPiezaDesdeFicheros(columnafinal.charAt(0), columnafinal.charAt(1));
						strings.add(columnafinal);
						piezas.add(p);
					}else {
						nohaypiezas = false;
					}
				} catch (ExcepcionPiezaDesconocida e) {
					nohaypiezas =false;
					System.err.println(e.getMessage()+ " 11 PartidaAjedrez");
				} catch (ExcepcionPosicionNoValida e) {
					System.err.println(e.getMessage()+ " 12 PartidaAjedrez");
				}
			} //FIn if Longitud
		} //Fin While Piezas
		
		if(piezas.isEmpty() && nohaypiezas) {
			inicializaTablero();
		}	
		else {		
			for(int i = 0; i < strings.size(); i++) {
				try {
					colocaPieza(strings.get(i));
				} catch (ExcepcionPiezaDesconocida e) {
					System.err.println(e.getMessage()+ " 13 PartidaAjedrez");
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage()+ " 14 PartidaAjedrez");
				} catch (ExcepcionPosicionNoValida e) {
					System.err.println(e.getMessage()+" 15 PartidaAjedrez");
				}
			}// for
		}
	}

	/**
	 * Coloca pieza.
	 *
	 * @param s La String donde esta la pieza.
	 * @throws ExcepcionPiezaDesconocida la excepcion pieza desconocida
	 * @throws ExcepcionCoordenadaErronea la excepcion coordenada erronea
	 * @throws ExcepcionPosicionNoValida la excepcion posicion no valida
	 */
	protected void colocaPieza(String s) throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida{
		if(isPiezaEnFicherosCorrecta(s)) {
			Coordenada c = new Coordenada(s.charAt(3),convertirCharNumaInt(s.substring(4, s.length()))); 
			Pieza p = crearPiezaDesdeFicheros(s.charAt(0), s.charAt(1));
			T.colocaPiezaAt(c, p);
			piezas.add(p);
		}
	}// Fin colocaPieza

	/**
	 * Comprueba si la pieza en ficheros es correcta.
	 *
	 * @param s la string a comprobar.
	 * @return true, si la pieza es correcta, sino false.
	 * @throws ExcepcionPiezaDesconocida la excepcion pieza desconocida
	 * @throws ExcepcionPosicionNoValida la excepcion posicion no valida
	 */
	private boolean isPiezaEnFicherosCorrecta(String s) throws ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {
		boolean ispieza = false;
		String aux = s.substring(0, s.indexOf(" ")); //Para comprobar que la pieza sea de dos carácteres.
		if (!isPiezaEnFicheros(s.charAt(0), s.charAt(1)) || aux.length() != 2) {
			throw new ExcepcionPiezaDesconocida(s.charAt(0));
		}
		try {
			if(Character.isDigit(s.charAt(4))) { // Si la coordenada no es LetraDígito, salta Error.
				Coordenada c = new Coordenada(s.charAt(3),convertirCharNumaInt(s.substring(4, s.length()))); // Creo coordenada que nos pasan

				if (convertirCharaInt(c.getLetra()) > T.getDimx() || c.getY() > T.getDimy()) { // Si la coordenada no pertenece al tablero.				
					throw new ExcepcionCoordenadaErronea(s.charAt(3), convertirCharNumaInt(s.substring(4, s.length())));
				}
				if (T.getCasillaAt(c).isOcupada()) { // Si la casilla está ocupada.
					throw new ExcepcionPosicionNoValida(c);
				}
				ispieza = true;
			}
			else {
				throw new ExcepcionCoordenadaErronea(s.charAt(3), -1);
			}
		} catch (ExcepcionCoordenadaErronea e) {
			System.err.println(e.getMessage()+ " 16 PartidaAjedrez");
		}

		return ispieza;
	}

	/**
	 * Convertir char a int en String.
	 *
	 * @param s La string
	 * @return El valor de la string
	 */
	private int convertirCharNumaInt(String s) {
		Integer i = new Integer(s).intValue();
		return i;
	}

	/**
	 * Convertir char a int.
	 *
	 * @param l El caracter a convertir
	 * @return el entero convertido
	 */
	private int convertirCharaInt(char l) {
		return ((int) (l - 'A'));
	}

	/**
	 * Comprueba si es una pieza en ficheros.
	 *
	 * @param p El caracter en mayuscula
	 * @param c El caracter en minuscula
	 * @return true, si los parametros corresponden a una pieza, sino false.
	 */
	private boolean isPiezaEnFicheros(char p, char c) { // Comprueba Si es pieza en lectura del fichero.
		boolean is = false;
		if (p == 'P' || p == 'R' || p == 'T' || p == 'C' || p == 'D' || p == 'A') {
			if (c == 'n' || c == 'b') {
				is = true;
			}
		}
		return is;
	}

	/**
	 * Crear pieza desde ficheros.
	 *
	 * @param p El caracter en mayuscula.
	 * @param c El caracter en minuscula.
	 * @return La pieza correspondiente a esos caracteres.
	 */
	private Pieza crearPiezaDesdeFicheros(char p, char c) {
		Pieza P = null;
		Color color;
		if (c == 'n') {
			color = Color.NEGRO;
		} else{
			color = Color.BLANCO;
		}
		try {
				P = FactoriaPieza.creaPieza(p, color);
		} catch (ExcepcionPiezaDesconocida e) {
			System.err.println(e.getMessage()+ " 17 PartidaAjedrez");
		}
		return P;
	}// Fin crearPiezaEnFicheros

	/**
	 * Convertir int a char.
	 *
	 * @param n El entero a convertir
	 * @return El char convertido.
	 */
	private char convertirIntaChar(int n) {
		return ((char) (n + 'A'));
	}
	

	/**
	 * Gets the num movimientos.
	 *
	 * @return El numero de movimientos que tiene la array de movimientos
	 */
	public int getNumMovimientos() {
		return movs.size();
	}
	
	/**
	 * Getter de las piezas que hay en el tablero del color proporcionado.
	 *
	 * @param c El color de las piezas que se buscara.
	 * @return Las piezas que tengan ese color y esten en la partida.
	 */
	public List<Pieza> getPiezas(Color c) {
		List<Pieza> LPieza = new ArrayList <Pieza> ();
		for( int i = 0; i < piezas.size(); i++){
			if(piezas.get(i).getColor() == c)
				LPieza.add(piezas.get(i));
		}
		return LPieza;
	}
	
	/**
	 * Coronar.
	 *
	 * @param peon El peon que se va a coronar
	 * @param pieza La pieza que va a sustituir
	 * @throws ExcepcionMovimientoCoronacion La excepcion Cororar.
	 */
	protected void coronar(Peon peon, Pieza pieza) throws ExcepcionMovimientoCoronacion {
		if(pieza == null || peon== null || peon.getCasilla() ==null || peon.getCasilla() == null) {
			throw new ExcepcionMovimientoCoronacion(peon,pieza);
		}else 
		{
			if(pieza.getTipo() == 'R' || pieza.getTipo() == 'P' || !peon.isMismoColor(pieza) || pieza.getCasilla() != null || !peon.isEnUltimaFila()) {
				throw new ExcepcionMovimientoCoronacion(peon, pieza);
			} else {
				try {
					T.colocaPiezaAt(peon.quitaDeCasilla().getCoordenada(), pieza);
				} catch (ExcepcionCoordenadaErronea e) {
					System.err.println(e.getMessage()+ " 18 PartidaAjedrez");
				}
			}
		}
	}
}
