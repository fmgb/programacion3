package modelo;

import java.util.HashSet;
import java.util.Set;

import modelo.excepciones.ExcepcionCoordenadaErronea;


/**
 * La Clase Rey. Hereda de Pieza.
 * @author Francisco Manuel García Botella 48570019-E
 */
public class Rey extends Pieza {
	
	/** El tipo de pieza. */
	public static final char tipo = 'R';

	/**
	 * Constructor del Rey.
	 *
	 * @param c El color que va a ser el rey.
	 */
	public Rey(Color c) {
		super(c); // Pasamos al padre el color.
		this.valor = 0.0;
	}
	

	/* (non-Javadoc)
	 * @see modelo.Pieza#isMismoTipo(modelo.Pieza)
	 */
	@Override
	public boolean isMismoTipo(Pieza p) {
		boolean is = false;
		if (p.getTipo() == tipo) {
			is = true;
		}
		return is;
	}

	/** Getter Tipo pieza.
	 * @return Devuelve el tipo de pieza.
	 */
	public char getTipo() {
		return tipo;
	}

	/* (non-Javadoc)
	 * @see modelo.Pieza#getCasillasAmenazadas()
	 */
	@Override
	public Set<Casilla> getCasillasAmenazadas() {
		Set<Casilla> Lista = new HashSet<Casilla> ();
		if(this.getCasilla() == null || !T.casillaEstaEnTablero(this.getCasilla().getCoordenada().getLetra(), this.getCasilla().getCoordenada().getY())) {
			System.err.println("La casilla de la pieza: " + this.toString() + " es nula.");
		}else {
			char Letra = this.getCasilla().getCoordenada().getLetra();
			int Y = this.getCasilla().getCoordenada().getY();
			Lista = getCasillasAmenazadasRey(Letra,Y);	
		}
		return Lista;
	}
	
	/**
	 * Getter Las casillas que amezanaza el rey.
	 *
	 * @param Letra La letra de la coordenada donde esta el Rey.
	 * @param Y El numero de la coordenada donde esta el rey.
	 * @return Las casillas que amenaza el rey.
	 */
	private Set<Casilla>getCasillasAmenazadasRey(char Letra,int Y) { //CHANGE
		Set<Casilla> Lista = new HashSet<Casilla>();
		
		int y [] = { +1, +1, +1,  0, -1, -1, -1,  0};
		int L [] = { -1,  0, +1, +1, +1,  0, -1, -1};
		try {
			for(int i = 0; i < L.length && i < y.length; i++) {
				if(T.casillaEstaEnTablero((char) (Letra + L[i]), Y + y[i])){
					Lista.add(T.getCasillaAt((char) (Letra + L[i]), Y + y[i]));
				}
			}
		}catch (ExcepcionCoordenadaErronea ce) {
			System.err.println(ce.getMessage());
		}
		return Lista;
	}
	
	/**
	 * Comprobar enroque, tanto largo como corto. El rey ni la torre no se deben haber movido nunca.
	 *
	 * @param c Casilla que estara la torre que se quiere hacer el enroque.
	 * @return Cierto si se puede hacer, Falso si no.
	 */
	public boolean comprobarEnroque(Casilla c) {
		boolean puedeEnrocar = false;
		if(!c.isNula() && this.getCasilla() != null && c != null) { // IF 1
			Pieza laTorre = c.getPieza();
			char Lr = this.getCasilla().getCoordenada().getLetra();
			char Lt = c.getCoordenada().getLetra();
			
			if(c.getPieza().getTipo() == 'T' && !this.haMovido() && !laTorre.haMovido() 
					&& this.isMismoColor(c.getPieza()) && Lr == 'E' && (Lt == 'A' || Lt == 'H')) { //IF 2
				if(Lt == 'A') {//IF 5
					if(comprobarEnroqueLargo(c)) { //IF 6
						puedeEnrocar = true;
					}else {//ELSE 6
						puedeEnrocar = false;
					}
				}else if (Lt == 'H') {//ELSEIF 5
					if(comprobarEnroqueCorto(c)) { //IF 6
						puedeEnrocar = true;
					}else { //ELSE 6
						puedeEnrocar = false;
					}
				}
			}else {//ELSE 2
				puedeEnrocar = false;
			}
		} else { // ELSE 1
			puedeEnrocar = false;
		}
		
		return puedeEnrocar;
	}
	
	/**
	 * Comprobar enroque corto.
	 *
	 * @param c Casilla donde esta la torre.
	 * @return Cierto si puede enrocar. Falso si no.
	 */
	private boolean comprobarEnroqueCorto(Casilla c) {
		int Yr = this.getCasilla().getCoordenada().getY();
		int Yt = c.getCoordenada().getY();
		try {
		if(this.getColor() == Color.BLANCO){
			if(Yr == 1 && Yt == 1 && !this.getCasilla().isAmenazada(Color.NEGRO)) {
				Casilla casF = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('F',1);
				Casilla casG = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('G',1);
				
				if(!casF.isOcupada() && !casG.isOcupada() && !casF.isAmenazada(Color.NEGRO) && !casG.isAmenazada(Color.NEGRO)) {
					return true;
				}
			}
		} else if (this.getColor() == Color.NEGRO){ 
			if(Yr == 8 && Yt == 8 && !this.getCasilla().isAmenazada(Color.BLANCO)) { 
				Casilla casF = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('F',8);
				Casilla casG = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('G',8);
				
				if(!casF.isOcupada() && !casG.isOcupada() && !casF.isAmenazada(Color.BLANCO) && !casG.isAmenazada(Color.BLANCO)) {
					return true;
				}
			}
		}
		} catch (ExcepcionCoordenadaErronea e) {
			System.err.println(e.getMessage());
		}
		return false;
	}

	/**
	 * Comprobar enroque largo.
	 *
	 * @param c Casilla donde esta la Torre.
	 * @return Cierto si se puede, falso si no.
	 */
	private boolean comprobarEnroqueLargo(Casilla c) {
		int Yr = this.getCasilla().getCoordenada().getY();
		int Yt = c.getCoordenada().getY();
		try {
		if(this.getColor() == Color.BLANCO){
			if(Yr == 1 && Yt == 1 && !this.getCasilla().isAmenazada(Color.NEGRO)) {
				Casilla casD = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('D',1);
				Casilla casC = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('C',1);
				Casilla casB = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('B',1);
				
				if(!casD.isOcupada() && !casC.isOcupada() && !casB.isOcupada() 
						&& !casD.isAmenazada(Color.NEGRO) && !casC.isAmenazada(Color.NEGRO)) {
					return true;
				}
			}
		}else if (this.getColor() == Color.NEGRO){
			if(Yr == 8 && Yt == 8 && !this.getCasilla().isAmenazada(Color.BLANCO)) {
				Casilla casD = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('D',8);
				Casilla casC = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('C',8);
				Casilla casB = PartidaAjedrez.getInstancia().getTablero().getCasillaAt('B',8);
				
				if(!casD.isOcupada() && !casC.isOcupada() && !casB.isOcupada() 
						&& !casD.isAmenazada(Color.BLANCO) && !casC.isAmenazada(Color.BLANCO)) {
					return true;
				}
			}
		}
		} catch (ExcepcionCoordenadaErronea e) {
			System.err.println(e.getMessage());
		}
		return false;
	}
}//FIN DE CLASE REY
