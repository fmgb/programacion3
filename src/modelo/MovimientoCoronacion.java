package modelo;


/**
 * La Clase Movimiento Coronacion. Hereda de la Clase Movimiento.
 * 
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class MovimientoCoronacion extends Movimiento {
	
	/** La pieza. */
	private Pieza pieza;
	
	/**
	 * Metodo MovimientoCoronacion.
	 *
	 * @param co Coordenada de donde viene el peon.
	 * @param cd Coordenada donde llega el peon
	 * @param P La pieza por la que tiene que cambiar.
	 */
	public MovimientoCoronacion(Coordenada co, Coordenada cd, Pieza P) {
		super(co, cd);
		pieza = P;
	}
	/**
	 * Metodo que devuelve la pieza.
	 * @return Devuelve la pieza que se cambia por el peon.
	 */
	public Pieza getPieza() {
		return pieza;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Movimiento Coronacion [co=" + co + ", cd=" + cd + "]" + "Pieza: " + this.getPieza();
	}
}
