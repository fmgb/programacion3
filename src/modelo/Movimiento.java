/**
 * @author Francisco Manuel García Botella, 48570019-E
 * 
 */
package modelo;



/**
 * La clase abstracta Movimiento.
 */
public abstract class Movimiento {


	/** La coordenada Origen. */
	protected Coordenada co;
	
	/** La coordenada Destino. */
	protected Coordenada cd;

	/**
	 * Constructor del Movimiento.
	 *
	 * @param co La coordenada origen.
	 * @param cd La coordenada destino.
	 */
	public Movimiento(Coordenada co, Coordenada cd) {
		this.co = new Coordenada(co);
		this.cd = new Coordenada(cd);
	}

	/**
	 * Getter.
	 *
	 * @return La coordenada origen
	 */
	public Coordenada getCoordenadaOrigen() { // Devuelve Coordenada Origen
		return co;
	}

	/**
	 * Getter.
	 *
	 * @return La coordenada destino
	 */
	public Coordenada getCoordenadaDestino() { // Devuelve Coordenada Destino.
		return cd;
	}
}
