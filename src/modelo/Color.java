package modelo;

/**
 * El Enum Color.
 * 
 * @author Francisco Manuel Garcia Botella 48570019-E
 */

public enum Color {

	/** El nulo. */
	NULO,

	/** El negro. */
	NEGRO,

	/** El blanco. */
	BLANCO;
}
