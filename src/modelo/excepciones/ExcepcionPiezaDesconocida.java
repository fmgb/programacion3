package modelo.excepciones;


/**
 * La Clase ExcepcionPiezaDesconocida.
 * @author Francisco Manuel García Botella 48570019-E
 */
@SuppressWarnings("serial")
public class ExcepcionPiezaDesconocida extends Exception {
	
	/** The letra. */
	private char letra;

	/**
	 * Constructor de la excepcion.
	 *
	 * @param letra the letra
	 */
	public ExcepcionPiezaDesconocida(char letra) {
		this.letra = letra;
	}

	/**
	 * Gets the letra.
	 *
	 * @return the letra
	 */
	public char getLetra() {
		return letra;
	}

	/**
	 * getMessage El mensaje.
	 *
	 * @return String del mensaje de la excepcion
	 */
	public String getMessage() {
		return "El carácter " + letra
				+ " no corresponde a ninguna pieza conocida";
	}
}
