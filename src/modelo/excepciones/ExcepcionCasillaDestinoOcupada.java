package modelo.excepciones;

import modelo.Movimiento;

/**
 * La Clase ExcepcionCasillaDestinoOcupada.
 * @author Francisco Manuel García Botella 48570019-E
 */
@SuppressWarnings("serial")
public class ExcepcionCasillaDestinoOcupada extends ExcepcionMovimiento {

	/**
	 * Instantiates a new excepcion casilla destino ocupada.
	 *
	 * @param m the m
	 */
	public ExcepcionCasillaDestinoOcupada(Movimiento m) {
		super(m);
	}

	/** el mensaje que imprime por pantalla.
	 * @return String del mensaje
	 */
	public String getMessage() {
		return "El movimiento no se puede realizar porque la casilla ("
				+ this.getMovimiento().getCoordenadaDestino().toString()
				+ ") está ocupada";
	}

}
