package modelo.excepciones;

import modelo.Movimiento;

/**
 * The Class ExcepcionCasillaOrigenVacia.
 * @author Francisco Manuel García Botella 48570019-E
 */
@SuppressWarnings("serial")
public class ExcepcionCasillaOrigenVacia extends ExcepcionMovimiento {

	/**
	 * Instantiates a new excepcion casilla origen vacia.
	 *
	 * @param m the m
	 */
	public ExcepcionCasillaOrigenVacia(Movimiento m) {
		super(m);
	}

	/** El mensaje que devuelve la excepción.
	 * @return La string del mensaje.
	 */
	public String getMessage() {
		return "La casilla (" + this.getMovimiento().getCoordenadaOrigen().toString() + ") (ORIGEN) está vacía";
	}
}
