package modelo.excepciones;

import modelo.Pieza;


/**
 * La clase ExcepcionMovimientoCoronacion.
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class ExcepcionMovimientoCoronacion extends ExcepcionMovimiento{


	/** La constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** El peon. */
	private Pieza peon;
	
	/** La pieza. */
	private Pieza pieza;
	
	/**
	 * Constructor de la excepcion.
	 *
	 * @param peon El peon que se cambiara.
	 * @param pieza LA pieza por la que se queria cambiar.
	 */
	public ExcepcionMovimientoCoronacion(Pieza peon, Pieza pieza){
		super(null);
		this.peon = peon;
		this.pieza = pieza;
	}
	
	/**
	 * Getter the pieza.
	 *
	 * @return La pieza que se queria cambiar.
	 */
	public Pieza getPieza() {
		return pieza;
	}
	
	/**
	 * Getter the peon.
	 *
	 * @return El peon que se cambiaba.
	 */
	public Pieza getPeon() {
		return peon;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return "No se ha podido efectuar la coronación del peón " + this.peon + " a la pieza " + this.pieza;
	}
}
