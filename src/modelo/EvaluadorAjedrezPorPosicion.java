package modelo;

import java.util.List;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCasillaOrigenVacia;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionNoExisteMovimiento;
import modelo.excepciones.ExcepcionTurnoDelContrario;

/**
 * The Class EvaluadorAjedrezPorPosicion.
 * @author Garcia Botella, Francisco Manuel  48570019-E
 */
public class EvaluadorAjedrezPorPosicion implements EvaluadorAjedrez {

	/* (non-Javadoc)
	 * @see modelo.EvaluadorAjedrez#evalua(modelo.PartidaAjedrez, modelo.Color)
	 */
	@Override
	public double evalua(PartidaAjedrez pa, Color c) { //TODO Hacer con los metodos ya implementados.
		double Resultado = 0.0;
		Tablero T = pa.getTablero();
		List<Pieza> piezas = pa.getPiezas(c);
		try {
			Resultado = Resultado + peonDoblado(T, c);
			Resultado = Resultado + casillasAmenazadasPorUnColor(T, c);
			Resultado = Resultado + piezasDefendidas(pa, c);
			for(int i = 0; i < piezas.size();i++){
				Resultado = Resultado + peonesAvanzados(piezas.get(i));
			}
		} catch (ExcepcionCoordenadaErronea e) {
			System.err.println(e.getMessage());
		}
		return Resultado;
	}

	/* (non-Javadoc)
	 * @see modelo.EvaluadorAjedrez#evaluaMovimiento(modelo.PartidaAjedrez, modelo.Color, int)
	 */
	@Override
	public double evaluaMovimiento(PartidaAjedrez pa, Color c, int n) throws ExcepcionNoExisteMovimiento, ExcepcionCasillaOrigenVacia, ExcepcionTurnoDelContrario, ExcepcionMovimientoIlegal, ExcepcionCasillaDestinoOcupada {
		pa.ejecutaMovimiento(n);
        return this.evalua(pa, c);
	}


	/**
	 * Peon doblado.
	 *
	 * @param T the t
	 * @param color the color
	 * @return the double
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public double peonDoblado(Tablero T,Color color) throws ExcepcionCoordenadaErronea { //CHANGE Cambiar pa por Tablero T si se puede.
		double Resultado = 0.0;
		int X = T.getDimx();
		char dimX = ((char) (X + 'A'));
		for(char i = 'A'; i < dimX; i++) {
			boolean PeonDoblado = false;
			for(int j = 1; j <= T.getDimy(); j++) {
				if(T.getCasillaAt(i,j).getPieza() != null) {
					Pieza pieza = T.getCasillaAt(i,j).getPieza();
					if(pieza.getTipo() == 'P' && pieza.getColor() == color && PeonDoblado) {
						Resultado = Resultado -1.0;
					}
					if(pieza.getTipo() == 'P' && pieza.getColor() == color) {
						PeonDoblado = true;
					}
				}
			}
		}
		return Resultado;
	}
	

	/**
	 * Casillas amenazadas por un color.
	 *
	 * @param T the t
	 * @param color the color
	 * @return the double
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	public double casillasAmenazadasPorUnColor(Tablero T,Color color) throws ExcepcionCoordenadaErronea {
		double Resultado = 0.0;
		int X = T.getDimx();
		char dimX = ((char) (X + 'A'));
		for(char i = 'A'; i < dimX; i++) {
			for(int j = 1; j <= T.getDimy(); j++) {
				if(T.getCasillaAt(i, j) != null) { 
					if(T.getCasillaAt(i, j).isAmenazada(color)) {
						Resultado = Resultado + 1.0;
					}
				}
			}
		}
		return Resultado;
	}
	

	/**
	 * Piezas defendidas.
	 *
	 * @param pa the pa
	 * @param c the c
	 * @return the double
	 */
	public double piezasDefendidas(PartidaAjedrez pa,Color c) {
		double Resultado = 0.0;
		List<Pieza> piezas = pa.getPiezas(c);
		for(int i = 0; i < piezas.size(); i++) {
			if(piezas.get(i).getCasilla() != null) {
				if(!pa.getTablero().getAmenazas(piezas.get(i).getCasilla(), c).isEmpty()){
					Resultado = Resultado + 1.0;
				}
			}
		}
		return Resultado;
	}
	

	/**
	 * Peones avanzados.
	 *
	 * @param p the p
	 * @return the double
	 */
	public double peonesAvanzados(Pieza p) {
		double Resultado = 0.0;
		if(p.getCasilla() != null) {
			if(p.getTipo() == 'P') {
				int Y = p.getCasilla().getCoordenada().getY();
				if(p.getColor() == Color.BLANCO){
					if(Y == 5) {
						Resultado = 1.0;
					} else if( Y == 6 || Y == 7 || Y == 8) {
						Resultado = 2.0;
					}
				} else {
					if(Y == 4) {
						Resultado = 1.0;
					} else if( Y == 3 || Y == 2 || Y == 1) {
						Resultado = 2.0;
					}
				}
			}
		}
		return Resultado;
	}
} // FIN DE LA CLASE EVALUADOR AJEDREZ POR POSICION
