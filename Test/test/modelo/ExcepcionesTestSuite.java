package test.modelo;

import static org.junit.Assert.*;

import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionCasillaOrigenVacia;
import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionMovimiento;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;
import modelo.excepciones.ExcepcionTurnoDelContrario;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import modelo.*;

/**
 * The Class ExcepcionesTestSuite.
 */
public class ExcepcionesTestSuite {

	/**
	 * Tes excepcion posicion no valida.
	 */
	@Test
	public final void tesExcepcionPosicionNoValida() {
		Coordenada c=null;
		try {
			c = new Coordenada('D', 2);
			throw new ExcepcionPosicionNoValida(c);
		} catch (Exception ex) {
			if (ex instanceof ExcepcionPosicionNoValida)
				assertEquals(c,((ExcepcionPosicionNoValida) ex).getCoordenada());
			else fail("Se esperaba ExcepcionPosicionNoValida, pero se capturo "+ex.getClass().getSimpleName());
		}
	}

	/**
	 * Tes excepcion pieza desconocida.
	 */
	@Test
	public final void tesExcepcionPiezaDesconocida() {
		try {
			throw new ExcepcionPiezaDesconocida('X');
		} catch (Exception ex) {
			if (ex instanceof ExcepcionPiezaDesconocida) 
				assertEquals('X',((ExcepcionPiezaDesconocida) ex).getLetra());
			else fail("Se esperaba ExcepcionPiezaDesconocida, pero se capturo "+ex.getClass().getSimpleName());
		}
	}

	/**
	 * Tes excepcion coordenada erronea.
	 */
	@Test
	public final void tesExcepcionCoordenadaErronea() {
		try {
			throw new ExcepcionCoordenadaErronea('+', 1000);
		} catch (Exception ex) {
			if (ex instanceof ExcepcionCoordenadaErronea) {
				assertEquals('+',((ExcepcionCoordenadaErronea) ex).getLetra());
				assertEquals(1000,((ExcepcionCoordenadaErronea) ex).getY());
			}	else fail("Se esperaba ExcepcionCoordenadaErronea, pero se capturo "+ex.getClass().getSimpleName());
		}
	}

	/**
	 * Tes excepcion casilla origen vacia.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void tesExcepcionCasillaOrigenVacia() throws ExcepcionCoordenadaErronea {
		Movimiento m=null;
		Coordenada co=null;
		Coordenada cd=null;
		try {
			co =new Coordenada('A',7);
			cd = new Coordenada('B',8);
			m = new MovimientoOrdinario(co,cd);
			throw new ExcepcionCasillaOrigenVacia(m);
		} catch (ExcepcionMovimiento ex) {
			if (ex instanceof ExcepcionCasillaOrigenVacia) {
				assertEquals(cd,((ExcepcionCasillaOrigenVacia) ex).getMovimiento().getCoordenadaDestino());
				assertEquals(co,((ExcepcionCasillaOrigenVacia) ex).getMovimiento().getCoordenadaOrigen());
			}	else fail("Se esperaba ExcepcionCasillaOrigenVacia, pero se capturo "+ex.getClass().getSimpleName());
		}
	}
	
	/**
	 * Tes excepcion casilla destino ocupada.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void tesExcepcionCasillaDestinoOcupada() throws ExcepcionCoordenadaErronea {
		Movimiento m=null;
		Coordenada co=null;
		Coordenada cd=null;
		try {
			co =new Coordenada('A',7);
			cd = new Coordenada('B',8);
			m = new MovimientoOrdinario(co,cd);
			throw new ExcepcionCasillaDestinoOcupada(m);
		} catch (ExcepcionMovimiento ex) {
			if (ex instanceof ExcepcionCasillaDestinoOcupada) {
				assertEquals(cd,((ExcepcionCasillaDestinoOcupada) ex).getMovimiento().getCoordenadaDestino());
				assertEquals(co,((ExcepcionCasillaDestinoOcupada) ex).getMovimiento().getCoordenadaOrigen());
			}	else fail("Se esperaba ExcepcionCasillaDestinoOcupada, pero se capturo "+ex.getClass().getSimpleName());
		}
	}

	/**
	 * Tes excepcion turno del contrario.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void tesExcepcionTurnoDelContrario() throws ExcepcionCoordenadaErronea {
		Movimiento m=null;
		Coordenada co=null;
		Coordenada cd=null;
		try {
			co =new Coordenada('A',7);
			cd = new Coordenada('B',8);
			m = new MovimientoOrdinario(co,cd);
			throw new ExcepcionTurnoDelContrario(m);
		} catch (ExcepcionMovimiento ex) {
			if (ex instanceof ExcepcionTurnoDelContrario) {
				assertEquals(cd,((ExcepcionTurnoDelContrario) ex).getMovimiento().getCoordenadaDestino());
				assertEquals(co,((ExcepcionTurnoDelContrario) ex).getMovimiento().getCoordenadaOrigen());
			}	else fail("Se esperaba ExcepcionTurnoDelContrario, pero se capturo "+ex.getClass().getSimpleName());
		}
	}

	/**
	 * Tes excepcion movimiento ilegal.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void tesExcepcionMovimientoIlegal() throws ExcepcionCoordenadaErronea {
		Movimiento m=null;
		Coordenada co=null;
		Coordenada cd=null;
		Pieza p=null;
		try {
			co =new Coordenada('A',7);
			cd = new Coordenada('B',8);
			m = new MovimientoOrdinario(co,cd);
			p = new Peon(Color.BLANCO);
			throw new ExcepcionMovimientoIlegal(p, m);
		} catch (ExcepcionMovimiento ex) {
			if (ex instanceof ExcepcionMovimientoIlegal) {
				assertEquals(cd,((ExcepcionMovimientoIlegal) ex).getMovimiento().getCoordenadaDestino());
				assertEquals(co,((ExcepcionMovimientoIlegal) ex).getMovimiento().getCoordenadaOrigen());
				assertEquals(p.getTipo(),((ExcepcionMovimientoIlegal) ex).getPieza().getTipo());
				assertEquals(p.getColor(),((ExcepcionMovimientoIlegal) ex).getPieza().getColor());
			}	else fail("Se esperaba ExcepcionMovimientoIlegal, pero se capturo "+ex.getClass().getSimpleName());
		}
	}

}
