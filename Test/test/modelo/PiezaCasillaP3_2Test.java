package test.modelo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;


import modelo.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Estos test comprueban que la asociación Pieza-Casilla se mantiene
 * sincronizada invocando los métodos Casilla.setPieza() y Casilla.quitaPieza()
 * @author pierre
 *
 */
public class PiezaCasillaP3_2Test {
	
	/** The p3. */
	Pieza p1, p2, p3;
	
	/** The c3. */
	Casilla c1,c2,c3;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		p1 = new Peon(Color.NULO); // Pieza no valida
		p2 = new Peon(Color.NEGRO);
		p3 = new Rey(Color.BLANCO);
	
		c1 = new Casilla(); // Casilla nula
		c2 = new Casilla(Color.BLANCO,new Coordenada('H',1));
		c3 = new Casilla(Color.NEGRO,new Coordenada('A',8));
	}

	/**
	 * Test set pieza1.
	 */
	@Test
	public final void testSetPieza1() {
		// Se debe establecer asociación bidireccional.
		assertTrue(c2.setPieza(p2));
		
		assertSame(c2,p2.getCasilla());
		assertSame(p2,c2.getPieza());		
	}

	/**
	 * Test set pieza2.
	 */
	@Test
	public final void testSetPieza2() {
		// Una vez asociada a una pieza, la casilla no se puede asociar a otra pieza sin previamente quitar la anterior.
		c2.setPieza(p2);
		
		assertFalse(c2.setPieza(p3));
		assertSame(c2,p2.getCasilla());
		assertSame(p2,c2.getPieza());		
	}

	/**
	 * Test set pieza3.
	 */
	@Test
	public final void testSetPieza3() {
		c2.setPieza(p2);
		
		// Una vez puesta la pieza en una casilla, no puedo intentar asociar otra casilla a esa pieza sin antes quitarla de la otra casilla 
		assertFalse(c3.setPieza(p2));
		assertSame(c2,p2.getCasilla());
		assertSame(p2,c2.getPieza());		
	}
	
	/**
	 * Test set pieza4.
	 */
	@Test
	public final void testSetPieza4() {		
		// Una pieza no valida no se puede colocar en una casilla 
		assertFalse(c2.setPieza(p1));
		assertNull(p1.getCasilla());
		assertNull(c2.getPieza());
	}

	/**
	 * Test set pieza5.
	 */
	@Test
	public final void testSetPieza5() {		
		// Una pieza no se puede colocar en una casilla nula
		assertFalse(c1.setPieza(p2));
		assertNull(p2.getCasilla());
		assertNull(c1.getPieza());
	}
	
	/**
	 * Test quita pieza1.
	 */
	@Test
	public final void testQuitaPieza1() {
		c2.setPieza(p2);
		// se elimina correctamente la asociacion bidireccional
		assertEquals(p2,c2.quitaPieza());
		assertNull(p2.getCasilla());
		assertNull(c2.getPieza());
	}

	/**
	 * Test quita pieza2.
	 */
	@Test
	public final void testQuitaPieza2() {
		// quitaPieza devuelve null si la pieza no esta en una casilla
		assertNull(c2.quitaPieza());
		assertNull(c2.getPieza());		
	}
}
