package test.modelo;

import static org.junit.Assert.*;
import modelo.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Estos test comprueban que la asociación Pieza-Casilla se mantiene sincronizada
 * Utilizando los métodos Pieza.setCasilla() y Pieza.quitaDeCasilla()
 * @author pierre
 *
 */
public class PiezaCasillaP3_1Test {

	/** The p3. */
	Pieza p1, p2, p3;
	
	/** The c3. */
	Casilla c1,c2,c3;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		p1 = new Peon(Color.NULO); // Pieza no valida
		p2 = new Peon(Color.NEGRO);
		p3 = new Rey(Color.BLANCO);
	
		c1 = new Casilla(); // Casilla nula
		c2 = new Casilla(Color.BLANCO,new Coordenada('H',1));
		c3 = new Casilla(Color.NEGRO,new Coordenada('A',8));
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test set casilla1.
	 */
	@Test
	public final void testSetCasilla1() {
		// Se debe establecer asociación bidireccional.
		assertTrue(p2.setCasilla(c2));
		
		assertSame(c2,p2.getCasilla());
		assertSame(p2,c2.getPieza());		
	}

	/**
	 * Test set casilla2.
	 */
	@Test
	public final void testSetCasilla2() {
		// Una vez puesta en una casilla, la pieza no se puede asociar a otra sin previamente quitarla.
		p2.setCasilla(c2);
		
		assertFalse(p2.setCasilla(c3));
		assertSame(c2,p2.getCasilla());
		assertSame(p2,c2.getPieza());		
	}

	/**
	 * Test set casilla3.
	 */
	@Test
	public final void testSetCasilla3() {
		p2.setCasilla(c2);
		
		// Una vez puesta la pieza en una casilla, no puedo intentar colocar otra pieza en esa casilla sin antes quitar la otra pieza 
		assertFalse(p3.setCasilla(c2));
		assertSame(c2,p2.getCasilla());
		assertSame(p2,c2.getPieza());		
	}
	
	/**
	 * Test set casilla4.
	 */
	@Test
	public final void testSetCasilla4() {		
		// Una pieza no valida no se puede colocar en una casilla 
		assertFalse(p1.setCasilla(c2));
		assertNull(p1.getCasilla());
		assertNull(c2.getPieza());
	}

	/**
	 * Test set casilla5.
	 */
	@Test
	public final void testSetCasilla5() {		
		// Una pieza no se puede colocar en una casilla nula
		assertFalse(p2.setCasilla(c1));
		assertNull(p2.getCasilla());
		assertNull(c1.getPieza());
	}
	
	/**
	 * Test quita de casilla1.
	 */
	@Test
	public final void testQuitaDeCasilla1() {
		p2.setCasilla(c2);

		// se elimina correctamente la asociacion bidireccional
		assertEquals(c2,p2.quitaDeCasilla());
		assertNull(p2.getCasilla());
		assertNull(c2.getPieza());
	}

	/**
	 * Test quita de casilla2.
	 */
	@Test
	public final void testQuitaDeCasilla2() {
		// quitaDeCasilla devuelve null si la pieza no esta en una casilla
		assertNull(p2.quitaDeCasilla());
		assertNull(p2.getCasilla());		
	}


}
