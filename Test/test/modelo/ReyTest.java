package test.modelo;

import static org.junit.Assert.*;

import modelo.Casilla;
import modelo.Color;
import modelo.PartidaAjedrez;
import modelo.Pieza;
import modelo.Tablero;
import modelo.excepciones.ExcepcionCasillaDestinoOcupada;
import modelo.excepciones.ExcepcionMovimientoIlegal;
import modelo.Peon;
import modelo.Rey;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class ReyTest.
 */
public class ReyTest {

	/** The pb. */
	Pieza r0, rn, pn, rb, pb;
	
	/** The c4. */
	Casilla c0,a2,a3,a4,a5,b3,b4,b5,b6,b7,c4;
	// Se necesita el tablero del juego, no podemos probar con casillas sueltas, pues
	// el movimiento de las piezas depende de la situación del juego (puedeMover() consulta el tablero, por ejemplo)
	/** The paj. */
	PartidaAjedrez paj;
	
	/** The tablero. */
	Tablero tablero;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		
		paj = PartidaAjedrez.getInstancia();
		tablero = paj.getTablero();

		r0 = new Rey(Color.NULO); // Pieza no valida
		pn = new Peon(Color.NEGRO);
		rn = new Rey(Color.NEGRO);
		pb = new Peon(Color.BLANCO);
		rb = new Rey(Color.BLANCO);
	
		c0 = new Casilla(); // Casilla nula
		a2 = tablero.getCasillaAt('A',2);
		a3 = tablero.getCasillaAt('A',3);
		a4 = tablero.getCasillaAt('A',4);
		a5 = tablero.getCasillaAt('A',5);
		b3 = tablero.getCasillaAt('B',3);
		b4 = tablero.getCasillaAt('B',4);
		b5 = tablero.getCasillaAt('B',5);
		b6 = tablero.getCasillaAt('B',6);
		b7 = tablero.getCasillaAt('B',7);
		c4 = tablero.getCasillaAt('C',4);
		
	}

	/**
	 * Tear down.
	 */
	@After
	public void tearDown() {
		r0.quitaDeCasilla();
		pb.quitaDeCasilla();
		pn.quitaDeCasilla();
		rb.quitaDeCasilla();
		rn.quitaDeCasilla();
	}
	

	/**
	 * Test peon nulo no puede mover.
	 */
	@Test
	public final void testPeonNuloNoPuedeMover() {
		assertFalse(r0.puedeMover(a2));
	}

	/**
	 * Test peon fuerade tablero no puede mover.
	 */
	@Test
	public final void testPeonFueradeTableroNoPuedeMover() {
		assertFalse(rb.puedeMover(a2));
	}

	/**
	 * Test casilla destino no valida.
	 */
	@Test
	public final void testCasillaDestinoNoValida() {
		rb.setCasilla(a2);
		assertFalse(rb.puedeMover(c0));
	}


	/**
	 * Test casilla destino no vacia.
	 */
	@Test
	public final void testCasillaDestinoNoVacia() {
		rb.setCasilla(a3);
		pb.setCasilla(a4);
		assertFalse(rb.puedeMover(a4));
		pb.quitaDeCasilla();
		pb.setCasilla(a2);
		assertFalse(rb.puedeMover(a2));
	}

	/**
	 * Test destino ilegal.
	 */
	@Test
	public final void testDestinoIlegal() {
		rb.setCasilla(a2);
		assertFalse(rb.puedeMover(b5));
	}

	/**
	 * Test peon no captura2.
	 */
	@Test
	public final void testPeonNoCaptura2() {
		// Mov. de captura, pero no hay pieza del contrario en destino
		rb.setCasilla(b3);
		pb.setCasilla(a4);
		assertFalse(rb.puedeMover(a4));
		pb.quitaDeCasilla();
		pb.setCasilla(c4);
		assertFalse(rb.puedeMover(c4));
	}

	// Movimientos OK
	/**
	 * Test mov ok1.
	 *
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 */
	@Test
	public final void testMovOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a2);
		assertTrue(rb.puedeMover(a3));		
		rn.setCasilla(b7);
		assertTrue(rn.puedeMover(b6));		
	}

	/**
	 * Test mov ok2.
	 *
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 */
	@Test
	public final void testMovOk2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a3);
		assertTrue(rb.puedeMover(b3));		
		rn.setCasilla(b5);
		assertTrue(rn.puedeMover(a5));		
	}

	/**
	 * Test mov ok3.
	 *
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 */
	@Test
	public final void testMovOk3() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a4);
		assertTrue(rb.puedeMover(b3));		
		rn.setCasilla(b5);
		assertTrue(rn.puedeMover(a4));		
	}

	/**
	 * Test mov ok4.
	 *
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 */
	@Test
	public final void testMovOk4() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(b3);
		assertTrue(rb.puedeMover(a2));		
		rn.setCasilla(a4);
		assertTrue(rn.puedeMover(b5));		
	}

	/**
	 * **********
	 * Capturas
	 * *********.
	 */
	
	@Test
	public final void testMovCapturaOk1() {
		// b3xa4
		rb.setCasilla(b3);
		pn.setCasilla(a4);
		assertTrue(rb.puedeMover(a4));
	}

	/**
	 * Test mov captura ok2.
	 */
	@Test
	public final void testMovCapturaOk2() {
		// b3xc4
		rn.setCasilla(b3);
		pb.setCasilla(c4);
		assertTrue(rn.puedeMover(c4));
	}
	
	//--------------- rey() ---------------- 
	/**
	 * Test mueve destino ocupado1.
	 *
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 */
	@Test(expected=ExcepcionCasillaDestinoOcupada.class)
	public final void testMueveDestinoOcupado1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a2);
		pb.setCasilla(a3);
		rb.mueve(a3);
	}


	/**
	 * Test movimiento ilegal.
	 *
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 */
	@Test(expected=ExcepcionMovimientoIlegal.class)
	public final void testMovimientoIlegal() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		rb.setCasilla(a2);
		rb.mueve(c4);
	}

	/**
	 * Test mov rey ok1.
	 *
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 */
	@Test
	public final void testMovReyOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		rb.setCasilla(a2);
		rb.mueve(a3);
		assertNull(a2.getPieza());
		assertSame(rb, a3.getPieza());
	}

	/**
	 * Test mov rey captura ok1.
	 *
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 */
	@Test
	public final void testMovReyCapturaOk1() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {		
		rb.setCasilla(b3);
		pn.setCasilla(a4);
		rb.mueve(a4);
		assertNull(b3.getPieza());
		assertSame(rb, a4.getPieza());
	}

	/**
	 * Test mov rey captura ok2.
	 *
	 * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
	 * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
	 */
	@Test
	public final void testMovReyCapturaOk2() throws ExcepcionCasillaDestinoOcupada, ExcepcionMovimientoIlegal {
		
		rn.setCasilla(b3);
		pb.setCasilla(c4);
		rn.mueve(c4);
		assertNull(b3.getPieza());
		assertSame(rn, c4.getPieza());
	}

	/**
	 * ***************
	 * OTROS METODOS
	 * ***************.
	 */
	
	@Test
	public final void testIsMismoTipo() {
		assertTrue(rb.isMismoTipo(rn));
		assertFalse(rb.isMismoTipo(pb));
	}

	/**
	 * Test get tipo.
	 */
	@Test
	public final void testGetTipo() {
		assertEquals('R',rb.getTipo());
	}

}
