package test.modelo;

import static org.junit.Assert.*;
import modelo.Color;
import modelo.Peon;
import modelo.Pieza;

import org.junit.*;

/**
 * The Class PiezaP2Test.
 */
public class PiezaP2Test {
	
	/** The p3. */
	Pieza p1, p2, p3;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		p1 = new Peon(Color.NULO);
		p2 = new Peon(Color.NEGRO);
		p3 = new Peon(Color.BLANCO);
		
	}
	
	/**
	 * Test colores.
	 */
	@Test
	public final void testColores()  {
		assertEquals("CL", Color.NULO, p1.getColor());
		assertEquals("CL", Color.NEGRO, p2.getColor());
		assertEquals("CL", Color.BLANCO, p3.getColor());
	}
	
	/**
	 * Test mismo color.
	 */
	@Test
	public final void testMismoColor()  {
		assertTrue("ColorAsiMismo", p1.isMismoColor(p1));
		assertFalse("ColorDistinto", p2.isMismoColor(p1));
	}
	
	/**
	 * Test is valida.
	 */
	@Test
	public final void testIsValida() {
		assertFalse("Falso p1", p1.isValida());
		assertTrue("Cierto p2", p2.isValida());
		assertTrue("Cierto p3", p3.isValida());
	}
	

}
