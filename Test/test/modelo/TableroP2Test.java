/**
 * 
 */
package test.modelo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.Peon;
import modelo.Tablero;
import modelo.Pieza;
import modelo.Coordenada;
import modelo.Color;
import modelo.excepciones.ExcepcionCoordenadaErronea;

/**
 * The Class TableroP2Test.
 *
 * @author gonzalo
 */
public class TableroP2Test {

	/** The t5. */
	Tablero t1,t2,t3,t4,t5;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		 t1 = new Tablero(0,2);
		 t2 = new Tablero(2,0);
		 t3 = new Tablero(7,4);
		 t4 = new Tablero(6,1);
		 t5 = new Tablero(27,8);
		
	}

	/**
	 * Test method for {@link modelo.Tablero#Tablero(int, int)}.
	 */
	@Test
	public final void testTableroIntInt() {
		// Prueba los filtros en las dimensiones del tablero
		assertEquals ("Tablero 8x2",8,t1.getDimx());
		assertEquals ("Tablero 8x2",2,t1.getDimy());
		assertEquals ("Tablero 2x8",8,t2.getDimy());
		assertEquals ("Tablero 8x4",8,t3.getDimx());
		assertEquals ("Tablero 6x8",8,t4.getDimy());
		assertEquals ("Tablero 8x8",8,t5.getDimx());	
	}

	/**
	 * Test method for {@link modelo.Tablero#Tablero(modelo.Tablero)}.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void testTableroTablero() throws ExcepcionCoordenadaErronea {
	
		Tablero aux = new Tablero(t5);
		// esto siempre es cierto: assertNotSame("No son el mismo", t1, aux);		
		
		//Prueba que tienen las mismas dimensiones
		assertEquals ("Misma dimx", t5.getDimx(), aux.getDimx());
		assertEquals ("Misma dimy", t5.getDimy(), aux.getDimy());
		
		// Comprueba la composicion Tablero-Casilla
		assertNotSame("Objetos Casilla A1 distintos", t5.getCasillaAt('A',1), aux.getCasillaAt('A',1));
		assertNotSame("Objetos Casilla A8 distintos", t5.getCasillaAt('A',8), aux.getCasillaAt('A',8));
		assertNotSame("Objetos Casilla H1 distintos", t5.getCasillaAt('H',1), aux.getCasillaAt('H',1));
		assertNotSame("Objetos Casilla H8 distintos", t5.getCasillaAt('H',8), aux.getCasillaAt('H',8));

	}

	/**
	 * Test method for {@link modelo.Tablero#getCasillaAt(modelo.Coordenada)}.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void testGetCasillaAtCoordenada() throws ExcepcionCoordenadaErronea {
		Coordenada coor;
		//Prueba Diagonal tablero Negras para getCasilla(Coordenada)
		for (int i=0; i<8; i++) {
			coor = new Coordenada( (char)((int)'A'+i),i+1 );
			assertEquals( "Diagonal negras",Color.NEGRO,(t5.getCasillaAt(coor)).getColor() );
		}
		
		//Prueba Diagonal tablero Blancas para getCasilla(Coordenada)
		int j=7;
		for (int i=0; i<8; i++) {
			coor = new Coordenada((char)((int)'A'+j),i+1 );
			assertEquals( "Diagonal blancas",Color.BLANCO,(t5.getCasillaAt(coor)).getColor() );
			j--;
		}
		
	}

	/**
	 * Test get casilla at coordenada fail.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test(expected=ExcepcionCoordenadaErronea.class)
	public void testGetCasillaAtCoordenadaFail()
			throws ExcepcionCoordenadaErronea {
		Coordenada coor;
		//Coordenada no valida
		coor = new Coordenada('C',9);
		t5.getCasillaAt(coor);
	}

	/**
	 * Test method for {@link modelo.Tablero#getCasillaAt(char, int)}.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void testGetCasillaAtCharInt() throws ExcepcionCoordenadaErronea {
		//Prueba Diagonal tablero Negras para getCasilla(char, int)
		for (int i=0; i<8; i++) {
			assertEquals( "Diagonal negras",Color.NEGRO,(t5.getCasillaAt((char)((int)'A'+i),i+1)).getColor() );
		}
		
//		Prueba Diagonal tablero Blancas para getCasilla(char, int)
		int j=7;
		for (int i=0; i<8; i++) {
			assertEquals( "Diagonal blancas",Color.BLANCO,(t5.getCasillaAt((char)((int)'A'+j),i+1)).getColor() );
			j--;
		}
	}

	/**
	 * Test get casilla at char int fail.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test(expected=ExcepcionCoordenadaErronea.class)
	public void testGetCasillaAtCharIntFail()
			throws ExcepcionCoordenadaErronea {
		//Coordenada no valida
		t5.getCasillaAt('A',0);
	}

	/**
	 * Test method for {@link modelo.Tablero#colocaPiezaAt(modelo.Coordenada, modelo.Pieza)}.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void testColocaPiezaAt() throws ExcepcionCoordenadaErronea {
		//Coloca piezas blancas sobre diagonal negras e intenta colocarla otra vez en casilla ocupada
		Coordenada coor;
		Pieza p;
		for (int i=0; i<8; i++) {
			coor = new Coordenada((char)((int)'A'+i),i+1);
			p= new Peon(Color.BLANCO);
			assertTrue(t5.colocaPiezaAt(coor, p));
			assertEquals( "Pone pieza en Diagonal negras",Color.BLANCO,(t5.getCasillaAt((char)((int)'A'+i),i+1)).getPieza().getColor() );
			// Como la asociación esta ya establecida, 
			// volver a colocar la misma pieza en la misma casilla devuelve cierto.
			assertTrue(t5.colocaPiezaAt(coor, p));
			// Sin embargo, devuelve falso (y no asocia los objetos pieza y casilla) 
			// si intentamos colocar una pieza distinta en la misma casilla
			assertFalse(t5.colocaPiezaAt(coor, new Peon(Color.BLANCO)));
		}
		
		//Intentamos colocar una pieza no valida y comprobamos que devuelve false y que no la pone.
		p = new Peon(Color.NULO);
		assertFalse( t5.colocaPiezaAt(new Coordenada('C',6), p) );
		assertNull( t5.getCasillaAt('C', 6).getPieza() );
				
	}

	/**
	 * Test coloca pieza at fail.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test(expected=ExcepcionCoordenadaErronea.class)
	public void testColocaPiezaAtFail() throws ExcepcionCoordenadaErronea {
		Pieza p;
		//Intentamos colocar una pieza en coordenadas no validas
		p = new Peon (Color.BLANCO);
		assertFalse( t5.colocaPiezaAt(new Coordenada('C',9), p));
	}


	/**
	 * Test to string.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void testToString() throws ExcepcionCoordenadaErronea {
		Coordenada c = new Coordenada ('D',1);
		Pieza p = new Peon(Color.NEGRO);
		t1.colocaPiezaAt(c, p);
		p = new Peon(Color.BLANCO);
		t1.colocaPiezaAt(new Coordenada('H',2), p);
		String s1 = new String("00000001\n00010000\n");
		assertEquals("ToString",s1,t1.toString());
	}

}
