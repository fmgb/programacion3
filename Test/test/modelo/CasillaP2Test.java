/**
 * 
 */
package test.modelo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import modelo.Casilla;
import modelo.Coordenada;
import modelo.Color;
import modelo.Peon;
import modelo.Pieza;
import modelo.excepciones.ExcepcionCoordenadaErronea;

/**
 * The Class CasillaP2Test.
 *
 * @author gonzalo
 */
public class CasillaP2Test {

	/** The c4. */
	Casilla c1, c2, c3, c4;
	
	/** The p4. */
	Coordenada p1,p2, p4;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		p1 = new Coordenada('A',5);
		p2 = new Coordenada('C',3);
		p4 = new Coordenada('F',7);
		
		c1 = new Casilla(Color.NULO,p1);
		c2 = new Casilla(Color.NEGRO, p2);
		c3 = new Casilla();
		c4 = new Casilla(Color.BLANCO, p4 );
	}

	/**
	 * Test method for {@link modelo.Casilla#Casilla(modelo.Color, modelo.Coordenada)}.
	 */
	@Test
	public void testCasillaColorCoordenada() {
		assertTrue("Color NEGRO",Color.NEGRO == c2.getColor());
		assertEquals("Coordenadas iguales", c2.getCoordenada(),p2 );
		//Comprobacion de la relacion de composicion casilla - coordenada
		assertNotSame ("No es la misma coordenada",c2.getCoordenada(),p2);
		
		assertFalse ("Casilla vacia", c2.isOcupada());
		
	}

	/**
	 * Test method for {@link modelo.Casilla#Casilla()}.
	 */
	@Test
	public void testCasilla() {
		assertFalse("Casilla vacia",c3.isOcupada());
		assertEquals("Coordenada error",Coordenada.coordenadaError,c3.getCoordenada());
	}

	/**
	 * Test method for {@link modelo.Casilla#getColor()}.
	 */
	@Test
	public void testGetColor() {
		assertEquals("MismoColor1",Color.NULO,c1.getColor());
		assertFalse("DistintoColor", c1.getColor()==c2.getColor());
		assertEquals("MismoColor2",c1.getColor(), c3.getColor());
	}

	/**
	 * Test method for {@link modelo.Casilla#setColor(modelo.Color)}.
	 */
	@Test
	public void testSetColor() {
		c3.setColor(Color.NEGRO);
		c1.setColor(Color.BLANCO);
		assertEquals("Color Negro",Color.NEGRO,c3.getColor());
		assertEquals("Color Blanco",Color.BLANCO,c1.getColor());
		assertFalse("Distinto color",c1.getColor()==c3.getColor());
	}

	/**
	 * Test method for {@link modelo.Casilla#getCoordenada()}.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public void testGetCoordenada() throws ExcepcionCoordenadaErronea {
		Coordenada cor = new Coordenada('A',5);
		
		assertEquals("Coordenadas iguales",cor,c1.getCoordenada());
		assertFalse("Distintas Coordenada",c2.getCoordenada().equals(cor));
	}

	/**
	 * Test method for {@link modelo.Casilla#setCoordenada(modelo.Coordenada)}.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test(expected=ExcepcionCoordenadaErronea.class)
	public void testSetCoordenada1() throws ExcepcionCoordenadaErronea {
		c2.setCoordenada('+',-3);
	}

	/**
	 * Test method for {@link modelo.Casilla#setCoordenada(modelo.Coordenada)}.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public void testSetCoordenada2() throws ExcepcionCoordenadaErronea {

		c2.setCoordenada('C',3);
		assertEquals("Coordenadas iguales",p2, c2.getCoordenada());		
		assertFalse("Distintas coordenadas",c1.getCoordenada().equals(c2.getCoordenada()));
	}

	/**
	 * Test method for {@link modelo.Casilla#getPieza()}.
	 */
	@Test
	public void testGetPieza() {
		assertFalse ("Casilla vacia ",c1.isOcupada());
		
		assertEquals("Piezas iguales null",c1.getPieza(),c3.getPieza());
		
	}

	/**
	 * Test method for {@link modelo.Casilla#setPieza(modelo.Pieza)}.
	 */
	@Test
	public void testSetPieza() {
		Pieza p1=new Peon(Color.BLANCO);
		Pieza p2=new Peon(Color.NEGRO);
		Pieza p3=new Peon(Color.NULO);
		
		assertTrue ("A c2 se le pone una pieza",c2.setPieza(p1));
	
		//Comprobacio relacion asociacion
		assertSame ("p1 es la misma de la c2",p1,c2.getPieza());
		
		// Volver a colocar la misma pieza en la misma casilla devuelve cierto
		// Necesario para mantener correctamente sincronizada la asociación bidireccional
		// desde Casilla (además de desde Pieza)
		assertTrue ("A c2 se le pone la misma pieza",c2.setPieza(p1));

		assertFalse("No pone pieza en casilla ocupada 1", c2.setPieza(p2));
		assertSame ("No pone pieza en casilla ocupada 2", p1, c2.getPieza());
		
		assertFalse("No pone pieza en casilla no valida 1", c3.setPieza(p1));
		assertFalse ("No pone pieza en casilla no valida 2", c3.isOcupada());
		
		
		assertFalse ("No pone pieza no valida 1", c4.setPieza(p3));
		assertFalse ("No pone pieza no valida 2", c4.isOcupada());
	}
		

	/**
	 * Test method for {@link modelo.Casilla#isOcupada()}.
	 */
	@Test
	public void testIsOcupada() {
		assertFalse("No tiene pieza",c2.isOcupada());
		c2.setPieza(new Peon(Color.BLANCO));
		assertTrue("Tiene pieza",c2.isOcupada());
		
	}

	/**
	 * Test method for {@link modelo.Casilla#isNula()}.
	 */
	@Test
	public void testIsNula() {
		assertTrue("Color nulo",c1.isNula());
		assertFalse("Color no nulo",c2.isNula());
		assertTrue("Color nulo",c3.isNula());
		
	}
	
	/**
	 * Test quitaPieza.
	 */
	@Test
	public void testQuitaPieza() {
		assertEquals ("No hay pieza",null,c1.quitaPieza());
		Pieza p1 = new Peon(Color.NEGRO);
		c2.setPieza(p1);
		assertSame ("Si hay pieza",p1,c2.quitaPieza());
		assertTrue ("No hay pieza",c2.getPieza() == null);	
	}
	
	/**
	 * Test casilla error.
	 */
	@Test
	public void testCasillaError() {
		assertEquals("Sin pieza",null,Casilla.CasillaError.getPieza());
		assertEquals("Coordenada letra 0",'0',Casilla.CasillaError.getCoordenada().getLetra());
		assertEquals("Coordenada y 0",0,Casilla.CasillaError.getCoordenada().getY());
	}
}

