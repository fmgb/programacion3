package test.modelo;

import static org.junit.Assert.*;

import modelo.excepciones.ExcepcionCoordenadaErronea;

import org.junit.Before;
import org.junit.Test;
import modelo.*;

/**
 * The Class CoordenadaP2Test.
 *
 * @author drizo
 * @date 22/08/2011
 */
public class CoordenadaP2Test {
	
	/** The c. */
	Coordenada c;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		c = new Coordenada('F', 5);		
	}

	/**
	 * Test getters.
	 */
	@Test
	public final void testGetters() {
		assertEquals("Letra", 'F', c.getLetra());
		assertEquals("y", 5, c.getY());
	}

	/**
	 * Test inicializacion.
	 */
	@Test
	public final void testInicializacion() {
		Coordenada c3 = new Coordenada();
		assertEquals("c3.x", '0', c3.getLetra());
		assertEquals("c3.y", 0, c3.getY());
		Coordenada c2 = new Coordenada(c);
		assertEquals("c2.letra", c2.getLetra(), c.getLetra());
		assertEquals("c2.y", c2.getY(), c.getY());
	}

	/**
	 * Test inicializacion2.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test(expected=ExcepcionCoordenadaErronea.class)
	public void testInicializacion2() throws ExcepcionCoordenadaErronea {
		Coordenada c3;
		c3 = new Coordenada('*',100);
	}
	
	/**
	 * Test equals.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public final void testEquals() throws ExcepcionCoordenadaErronea {
		Coordenada c4 = new Coordenada('F',5);
		Coordenada c5 = new Coordenada('F',6);
		String s = new String();
		assertFalse(c.equals(null));
		assertFalse(c.equals(s));
		assertFalse(c.equals(c5));
		assertTrue(c.equals(c));
		assertTrue(c.equals(c4));
	
	}
	
	/**
	 * Test coordenada error.
	 */
	@Test
	public final void testCoordenadaError() {
		assertEquals("coordenadaError.getLetra()",Coordenada.coordenadaError.getLetra(),'0');
		assertEquals("coordenadaError.getY()",Coordenada.coordenadaError.getY(),0);
	}
	
	/**
	 * Test to string.
	 */
	@Test
	public final void testToString() {
		assertEquals("Coordenada.toString()",c.toString(),"F5");
		assertEquals("Coordenada.toString()",Coordenada.coordenadaError.toString(),"00");
	}
	
	
	
}
