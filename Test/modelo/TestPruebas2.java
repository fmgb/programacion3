/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/*
 * Para que te te pase el test correctamente, te tiene que pasar todas las 
 * pruebas, ademas debes comprobar la salida para corroborar que funciona
 * cogiendo un tablero vacio en papel y poner las piezas que se añaden en
 * creaPiezasTest e ir comprobando una a una
 * 
 * Para comprobar el inicializaTablero hay que poner todo el test en comentario 
 * y sacar el comentario del test inicilizaTableroTest
 * 
 */
import modelo.excepciones.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * The Class TestPruebas2.
 *
 * @author Almira Molla, Alejandro
 */
public class TestPruebas2 {

    /** The f. */

    
    /** The paj. */
    static PartidaAjedrez paj = PartidaAjedrez.getInstancia();;
    
    /** The tab. */
    static Tablero tab = paj.getTablero();

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {

    }
    
   /*  @Test //COMPROBADO
     public void inicializaTableroTest() throws ExcepcionCoordenadaErronea{
     // Tienes que cambiar inicializaTablero a 'public'
     paj.inicializaTablero();
     System.out.println(paj.toString());
     System.out.println();
     for(int i=0;i<paj.getPiezas(Color.BLANCO).size();i++){
     paj.getPiezas(Color.BLANCO).get(i).quitaDeCasilla();
     paj.getPiezas(Color.NEGRO).get(i).quitaDeCasilla();
     }
     System.out.println(paj.toString());
     }*/

    /**
    * Crea piezas test.
    *
    * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
    */
   @BeforeClass //COMPROBADO
    public static void creaPiezasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {
        System.out.println("creaPiezaTest");
        paj.colocaPieza("Pb G8");
        paj.colocaPieza("Pn D1");
        paj.colocaPieza("Pb D5");
        paj.colocaPieza("Pb A5");
        paj.colocaPieza("Pn H5");
        paj.colocaPieza("Pn A8");
        paj.colocaPieza("Pn H8");
        paj.colocaPieza("Pn H7");
        paj.colocaPieza("Rb A1");
        paj.colocaPieza("Rn H1");
        paj.colocaPieza("Rb E6");
        paj.colocaPieza("Pn D3");
        paj.colocaPieza("Pb C2");
        paj.colocaPieza("Tb F3");
        paj.colocaPieza("Cn C4");
        paj.colocaPieza("Ab F2");
        paj.colocaPieza("Db G5");
        System.out.println(paj.toString());
      //  Coordenada c = new Coordenada('C',2);
    //    tab.getCasillaAt(c).quitaPieza();
    }

        /**
         * Factoria test.
         *
         * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
         */
        @Test
    public void factoriaTest() throws ExcepcionPiezaDesconocida { //COMPROBADO

        System.out.println("factoriaTest");
        Pieza rb = new Rey(Color.BLANCO);
        Pieza rn = new Rey(Color.NEGRO);
        Pieza pb = new Peon(Color.BLANCO);
        Pieza pn = new Peon(Color.NEGRO);
        Pieza tb = new Torre(Color.BLANCO);
        Pieza tn = new Torre(Color.NEGRO);
        Pieza ab = new Alfil(Color.BLANCO);
        Pieza an = new Alfil(Color.NEGRO);
        Pieza cb = new Caballo(Color.BLANCO);
        Pieza cn = new Caballo(Color.NEGRO);
        Pieza db = new Dama(Color.BLANCO);
        Pieza dn = new Dama(Color.NEGRO);

        assertEquals("Crea un reyb", rb, FactoriaPieza.creaPieza('R', Color.BLANCO));
        assertEquals("Crea un reyn", rn, FactoriaPieza.creaPieza('R', Color.NEGRO));
        assertEquals("Crea un peonb", pb, FactoriaPieza.creaPieza('P', Color.BLANCO));
        assertEquals("Crea un peonn", pn, FactoriaPieza.creaPieza('P', Color.NEGRO));
        assertEquals("Crea un torreb", tb, FactoriaPieza.creaPieza('T', Color.BLANCO));
        assertEquals("Crea un torren", tn, FactoriaPieza.creaPieza('T', Color.NEGRO));
       assertEquals("Crea un alfilb", ab, FactoriaPieza.creaPieza('A', Color.BLANCO));
        assertEquals("Crea un alfiln", an, FactoriaPieza.creaPieza('A', Color.NEGRO));
        assertEquals("Crea un caballob", cb, FactoriaPieza.creaPieza('C', Color.BLANCO));
        assertEquals("Crea un caballon", cn, FactoriaPieza.creaPieza('C', Color.NEGRO));
        assertEquals("Crea un damab", db, FactoriaPieza.creaPieza('D', Color.BLANCO));
        assertEquals("Crea un daman", dn, FactoriaPieza.creaPieza('D', Color.NEGRO));
        System.err.println("CORRECTOFactoriaText");
    }


   @Test
    public void getPiezasTest() throws ExcepcionCoordenadaErronea { //CORRECTO
        System.out.print("getPiezasTest");
        System.out.println("Hay " + paj.getPiezas(Color.BLANCO).size() + " Piezas BLANCAS y "+ paj.getPiezas(Color.NEGRO).size() + " Piezas NEGRAS");
        System.out.println(paj.getPiezas(Color.BLANCO));
        System.err.println("CORRECTOGetPiezasTest");
    }
   
    /**
     * Peon is en ultima fila test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
     * @throws ExcepcionPosicionNoValida the excepcion posicion no valida
     */
    @Test
    public void peonIsEnUltimaFilaTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {

        System.out.println("peonIsEnUltimaFilaTest");

        Coordenada g8 = new Coordenada('G', 8);
        Coordenada d1 = new Coordenada('D', 1);
        Coordenada d5 = new Coordenada('D', 5);

        Peon pb1 = (Peon) paj.getPiezaAt(g8);
        Peon pn2 = (Peon) paj.getPiezaAt(d1);
        Peon pb2 = (Peon) paj.getPiezaAt(d5);

        System.out.println(pb1);

        assertTrue("PeonB en ultimaFila", pb1.isEnUltimaFila());
        assertTrue("PeonN en ultimaFila", pn2.isEnUltimaFila());
        assertFalse("PeonB no en ultimaFila", pb2.isEnUltimaFila());
        System.err.println("CORRECTO peonISEnUltimaFilaTest");
    }
    
   /**
    * Filas casillas amenazadas test.
    *
    * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
    * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
    * @throws ExcepcionPosicionNoValida the excepcion posicion no valida
    */
   @Test
    public void FilasCasillasAmenazadasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {


        Coordenada cor1 = new Coordenada('H', 3);
        Casilla cas1 = new Casilla(Color.NEGRO, cor1);

        //paj.colocaPieza("Pb D5");

        System.out.println("FilasCasillasAmenazadasTest desde: " + cor1);
        System.out.print("N: ");
        for (int i = 0; i < tab.getColumnaArriba(cas1).size(); i++) {
            System.out.print(tab.getColumnaArriba(cas1).get(i).getCoordenada() + " ");
        }

        System.out.println();
        System.out.print("S: ");
        for (int i = 0; i < tab.getColumnaAbajo(cas1).size(); i++) {
        	System.out.print(tab.getColumnaAbajo(cas1).get(i).getCoordenada() + " ");
        }
   
        System.out.println();
        System.out.print("O: ");
        for (int i = 0; i < tab.getFilaIzquierda(cas1).size(); i++) {
            System.out.print(tab.getFilaIzquierda(cas1).get(i).getCoordenada() + " ");
        }

        System.out.println();
        System.out.print("E: ");
        for (int i = 0; i < tab.getFilaDerecha(cas1).size(); i++) {
            System.out.print(tab.getFilaDerecha(cas1).get(i).getCoordenada() + " ");
        }
        System.out.println();
        //assertEquals("[Casilla [color=BLANCO, pieza=null, coordenada=C4], Casilla [color=NEGRO, pieza=null, coordenada=D4], Casilla [color=BLANCO, pieza=null, coordenada=E4], Casilla [color=NEGRO, pieza=Peon [getTipo()=P, getColor()=BLANCO, getCoordenada()=F4], coordenada=F4]]",tab.getFilaDerecha(cas1).toString());
    }
    
    /**
     * Diagonal casillas amenazadas test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
     * @throws ExcepcionPosicionNoValida the excepcion posicion no valida
     */
    @Test
    public void DiagonalCasillasAmenazadasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {

        Coordenada cor1 = new Coordenada('C', 2);
        Casilla cas1 = new Casilla(Color.NEGRO, cor1);

        System.out.println("DiagonalCasillasAmenazadasTest desde: " + cor1);
        System.out.print("NO: ");
        for (int i = 0; i < tab.getDiagonalNO(cas1).size(); i++) {
            System.out.print(tab.getDiagonalNO(cas1).get(i).getCoordenada() + " ");
        }

        System.out.println();
        System.out.print("NE: ");
        for (int i = 0; i < tab.getDiagonalNE(cas1).size(); i++) {
            System.out.print(tab.getDiagonalNE(cas1).get(i).getCoordenada() + " ");
        }

        System.out.println();
        System.out.print("SO: ");
        for (int i = 0; i < tab.getDiagonalSO(cas1).size(); i++) {
            System.out.print(tab.getDiagonalSO(cas1).get(i).getCoordenada() + " ");
        }

        System.out.println();
        System.out.print("SE: ");
        for (int i = 0; i < tab.getDiagonalSE(cas1).size(); i++) {
            System.out.print(tab.getDiagonalSE(cas1).get(i).getCoordenada() + " ");
        }
        System.out.println();
        //assertEquals("[Casilla [color=BLANCO, pieza=null, coordenada=C4], Casilla [color=NEGRO, pieza=null, coordenada=D4], Casilla [color=BLANCO, pieza=null, coordenada=E4], Casilla [color=NEGRO, pieza=Peon [getTipo()=P, getColor()=BLANCO, getCoordenada()=F4], coordenada=F4]]",tab.getFilaDerecha(cas1).toString());
    }

    @Test
    public void getSaltosCaballoTest() throws ExcepcionCoordenadaErronea {

        Coordenada cor1 = new Coordenada('H', 8);
        Casilla cas1 = new Casilla(Color.NEGRO, cor1);
        System.out.println("getSaltosCaballoTest desde:" + cor1);
        for (int i = 0; i < tab.getSaltosCaballo(cas1).size(); i++) {
            System.out.print(tab.getSaltosCaballo(cas1).get(i).getCoordenada() + " ");
        }
    }

    /**
     * Peonget casillas amenazadas test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
     * @throws ExcepcionPosicionNoValida the excepcion posicion no valida
     */
    @Test
    public void peongetCasillasAmenazadasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {

        System.out.print("peongetCasillasAmenazadasTest desde: ");
        Coordenada a5 = new Coordenada('A', 5);
        System.out.println(a5 + " " + paj.getPiezaAt(a5).getColor());
        System.out.println(paj.getPiezaAt(a5).getCasillasAmenazadas());
        Coordenada h7 = new Coordenada('H', 7);
        System.out.println(h7 + " " + paj.getPiezaAt(h7).getColor());
        System.out.println(paj.getPiezaAt(h7).getCasillasAmenazadas());
        Coordenada a8 = new Coordenada('A', 8);
        System.out.println(a8 + " " + paj.getPiezaAt(a8).getColor());
        System.out.println(paj.getPiezaAt(a8).getCasillasAmenazadas());
        Coordenada h8 = new Coordenada('H', 8);
        System.out.println(h8 + " " + paj.getPiezaAt(h8).getColor());
        System.out.println(paj.getPiezaAt(h8).getCasillasAmenazadas());
        Coordenada d5 = new Coordenada('D', 5);
        System.out.println(d5 + " " + paj.getPiezaAt(d5).getColor());
        System.out.println(paj.getPiezaAt(d5).getCasillasAmenazadas());
        Coordenada g8 = new Coordenada('G', 8);
        System.out.println(g8 + " " + paj.getPiezaAt(g8).getColor());
        System.out.println(paj.getPiezaAt(g8).getCasillasAmenazadas());
    }

    /**
     * Reyget casillas amenazadas test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
     * @throws ExcepcionPosicionNoValida the excepcion posicion no valida
     */
    @Test
    public void reygetCasillasAmenazadasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {

        System.out.print("reygetCasillasAmenazadasTest desde: ");
        Coordenada a1 = new Coordenada('A', 1);
        System.out.println(a1 + " " + paj.getPiezaAt(a1).getColor());
        System.out.println(paj.getPiezaAt(a1).getCasillasAmenazadas());
        Coordenada h1 = new Coordenada('H', 1);
        System.out.println(h1 + " " + paj.getPiezaAt(h1).getColor());
        System.out.println(paj.getPiezaAt(h1).getCasillasAmenazadas());
        Coordenada e6 = new Coordenada('E', 6);
        System.out.println(e6 + " " + paj.getPiezaAt(e6).getColor());
        System.out.println(paj.getPiezaAt(e6).getCasillasAmenazadas());

    }
    
    @Test
    public void getAmenazasTest() throws ExcepcionCoordenadaErronea {
        System.out.println("getAmenazasTest");
        Coordenada cor = new Coordenada('D', 3);
        Casilla cas = tab.getCasillaAt(cor);
        System.out.println("Amenazo: " + cor + " con las piezas BLANCAS");
        System.out.println(tab.getAmenazas(cas, Color.BLANCO));
    }

    /**
     * Checks if is amenazada test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     */
    @Test
    public void isAmenazadaTest() throws ExcepcionCoordenadaErronea {
        System.out.println("getAmenazasTest");
        Coordenada cor = new Coordenada('D', 3);
        Casilla cas = tab.getCasillaAt(cor);
        assertTrue("esAmenazada", cas.isAmenazada(Color.BLANCO));
        assertFalse("NoesAmenazada", cas.isAmenazada(Color.NEGRO));
    }

    /**
     * Torre test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     */
    @Test
    public void torreTest() throws ExcepcionCoordenadaErronea {
        System.out.println("torreTest");
        Coordenada f3 = new Coordenada('F', 3);
        Coordenada c3 = new Coordenada('C', 3);
        Coordenada f6 = new Coordenada('F', 6);

        Torre t = (Torre) paj.getPiezaAt(f3);
        assertEquals("tipoTorre", 'T', t.getTipo());
        assertEquals("valorTorre", 5, (int) t.getValor());
        System.out.print("torregetCasillasAmenazadasTest desde: ");
        System.out.println(f3 + " " + t.getColor());
        System.out.println(t.getCasillasAmenazadas());
        assertTrue("puedeMover", t.puedeMover(tab.getCasillaAt(f6)));
        assertFalse("NOpuedeMover", t.puedeMover(tab.getCasillaAt(c3)));

    }

      /**
       * Caballo test.
       *
       * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
       */
      @Test
    public void caballoTest() throws ExcepcionCoordenadaErronea {
        System.out.println("caballoTest");
        Coordenada c4 = new Coordenada('C', 4);
        Coordenada d6 = new Coordenada('D', 6);
        Coordenada c3 = new Coordenada('C', 3);

        Caballo t = (Caballo) paj.getPiezaAt(c4);
        assertEquals("tipoCaballo", 'C', t.getTipo());
        assertEquals("valorCaballo", 3, (int) t.getValor());
        System.out.print("caballogetCasillasAmenazadasTest desde: ");
        System.out.println(c4 + " " + t.getColor());
        System.out.println(t.getCasillasAmenazadas());
        assertTrue("puedeMover", t.puedeMover(tab.getCasillaAt(d6)));
        assertFalse("NOpuedeMover", t.puedeMover(tab.getCasillaAt(c3)));

    }

    /**
     * Alfil test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     */
    @Test
    public void alfilTest() throws ExcepcionCoordenadaErronea {
        System.out.println("alfilTest");
        Coordenada f2 = new Coordenada('F', 2);
        Coordenada a7 = new Coordenada('A', 7);
        Coordenada f1 = new Coordenada('F', 1);

        Alfil t = (Alfil) paj.getPiezaAt(f2);
        assertEquals("tipoAlfil", 'A', t.getTipo());
        assertEquals("valorAlfil", 3, (int) t.getValor());
        System.out.print("alfilgetCasillasAmenazadasTest desde: ");
        System.out.println(f2 + " " + t.getColor());
        System.out.println(t.getCasillasAmenazadas());
        assertTrue("puedeMover", t.puedeMover(tab.getCasillaAt(a7)));
        assertFalse("NOpuedeMover", t.puedeMover(tab.getCasillaAt(f1)));

    }

    /**
     * Dama test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     */
    @Test
    public void damaTest() throws ExcepcionCoordenadaErronea {
        System.out.println("damaTest");
        Coordenada g5 = new Coordenada('G', 5);
        //Coordenada a7 = new Coordenada('A', 7);
        //Coordenada f1 = new Coordenada('F', 1);

        Dama t = (Dama) paj.getPiezaAt(g5);
        assertEquals("tipoDama", 'D', t.getTipo());
        assertEquals("valorDama", 9, (int) t.getValor());
        System.out.print("damagetCasillasAmenazadasTest desde: ");
        System.out.println(g5 + " " + t.getColor());
        System.out.println(t.getCasillasAmenazadas());
        //assertTrue("puedeMover", t.puedeMover(tab.getCasillaAt(a7)));
        //assertFalse("NOpuedeMover", t.puedeMover(tab.getCasillaAt(f1)));

    }

    /**
     * Ha movido test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionMovimientoIlegal the excepcion movimiento ilegal
     * @throws ExcepcionCasillaDestinoOcupada the excepcion casilla destino ocupada
     */
    @Test
    public void haMovidoTest() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoIlegal, ExcepcionCasillaDestinoOcupada {

        System.out.println("haMovidoTest");
        Coordenada co = new Coordenada('A', 1);
        Coordenada co2 = new Coordenada('A', 2);
        Casilla c = tab.getCasillaAt(co);
        Casilla c2 = tab.getCasillaAt(co2);
        Pieza r1 = paj.getPiezaAt(co);
        assertFalse("No ha movido", r1.haMovido());
        //assertFalse("No ha movido por primera vez",r1.gethaMovidoP());
        r1.mueve(c2);
        assertTrue("Ha movido", r1.haMovido());
        r1.mueve(c);
        assertTrue("Ha movido aunque vuelva a la casilla original", r1.haMovido());
        //assertTrue("Si que ha movido alguna vez",r1.gethaMovidoP());

    }

    /**
     * Coronar fallo color test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionMovimientoCoronacion the excepcion movimiento coronacion
     */
    @Test(expected = ExcepcionMovimientoCoronacion.class)
    public void coronarFalloColorTest() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
        System.out.println("coronarFalloColorTest");
        Coordenada g8 = new Coordenada('G', 8);
        Pieza pieza = new Torre(Color.NEGRO);
        Peon peon = (Peon) paj.getPiezaAt(g8);
        System.out.println(paj);
        paj.coronar(peon, pieza);
        System.out.println(paj);
    }

    /**
     * Coronar fallo tipo test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionMovimientoCoronacion the excepcion movimiento coronacion
     */
    @Test(expected = ExcepcionMovimientoCoronacion.class)
    public void coronarFalloTipoTest() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
        System.out.println("coronarFalloTipoTest");
        Coordenada g8 = new Coordenada('G', 8);
        Pieza pieza = new Rey(Color.BLANCO);
        Peon peon = (Peon) paj.getPiezaAt(g8);
        System.out.println(paj);
        paj.coronar(peon, pieza);
        System.out.println(paj);
    }
    
    /**
     * Coronar fallo pieza tablero test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionMovimientoCoronacion the excepcion movimiento coronacion
     */
    @Test(expected = ExcepcionMovimientoCoronacion.class)
    public void coronarFalloPiezaTableroTest() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
        System.out.println("coronarFalloPiezaTableroTest");
        Coordenada g8 = new Coordenada('G', 8);
        Coordenada f3 = new Coordenada('F', 3);
        Pieza pieza = paj.getPiezaAt(f3);
        Peon peon = (Peon) paj.getPiezaAt(g8);
        System.out.println(paj);
        paj.coronar(peon, pieza);
        System.out.println(paj);
    }
    
    @Test
    public void coronarTest() throws ExcepcionCoordenadaErronea, ExcepcionMovimientoCoronacion {
        System.out.println("coronarTest");
        Coordenada g8 = new Coordenada('G', 8);
        Coordenada f3 = new Coordenada('F', 3);
        Pieza pieza = new Torre(Color.BLANCO);
        Peon peon = (Peon) paj.getPiezaAt(g8);
        System.out.println(paj);
        paj.coronar(peon, pieza);
        System.out.println(paj);
    }    
}
