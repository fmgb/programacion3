package modelo;

import static org.junit.Assert.*;

import java.util.List;

import modelo.excepciones.ExcepcionCoordenadaErronea;

import org.junit.Before;
import org.junit.Test;

/**
 * The Class EvaluadorAjedrezPorPosicionTest.
 */
public class EvaluadorAjedrezPorPosicionTest {

	/** The paj. */
	PartidaAjedrez paj;
	
	/** The t. */
	Tablero T;
	
	/** The e. */
	EvaluadorAjedrezPorPosicion E = new EvaluadorAjedrezPorPosicion();
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		paj = PartidaAjedrez.getInstancia();
		T = paj.getTablero();
		paj.getInstancia().inicializaTablero();

//		System.out.println(T.toString());

	}

	/**
	 * Test evalua.
	 */
	@Test
	public void testEvalua() {
		
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testPeonDoblado() throws ExcepcionCoordenadaErronea {
		double peonDobladoNO = 0.0, peonDobladoSI = -1.0;
		Coordenada C4 = new Coordenada('C',4);
		Pieza P = new Peon(Color.BLANCO);	
		Coordenada D4 = new Coordenada('D',4);
		Pieza p = new Peon(Color.NEGRO);
		Coordenada C3 = new Coordenada('C',3);
		Coordenada C6 = new Coordenada('C',6);
		Coordenada A4 = new Coordenada('A',4);
		Coordenada E6 = new Coordenada('E',6);
		Pieza P1 = new Peon(Color.BLANCO);	
		Pieza P2 = new Peon(Color.BLANCO);	
		Pieza P3 = new Torre(Color.BLANCO);	
		Pieza P4 = new Dama(Color.BLANCO);	
		Pieza P5 = new Alfil(Color.BLANCO);
		T.colocaPiezaAt(C4, P);
		T.colocaPiezaAt(D4, p);
		T.colocaPiezaAt(C3, P1);

		assertEquals("Hola", -2, (int)E.peonDoblado(paj.getTablero(), Color.BLANCO));
		T.colocaPiezaAt(C6, P2);
		assertEquals("Hola", -3, (int)E.peonDoblado(paj.getTablero(), Color.BLANCO));
		System.out.println(paj.getInstancia().toString());
		System.out.println(E.peonDoblado(paj.getInstancia().getTablero(), Color.BLANCO)+ " DEBE DE SER -1.0" );
//		System.out.println(E.peonDoblado(paj, Color.NEGRO)+ " DEBE DE SER -1.0" );
		//P.quitaDeCasilla();	
		//p.quitaDeCasilla();
	//	System.out.println(E.peonDoblado(paj, Color.BLANCO)+ " DEBE DE SER 0.0" );
//		System.out.println(E.peonDoblado(paj, Color.NEGRO)+ " DEBE DE SER 0.0" );
	//	T.colocaPiezaAt(C4, p);
	}

	/**
	 * Test casillas amenazadas por un color.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	/*	@Test
	public void testCasillasAmenazadasPorUnColor() throws ExcepcionCoordenadaErronea {
		List<Pieza> piezas = paj.getInstancia().piezas;
		Coordenada C4 = new Coordenada('C',4);
		Coordenada B3 = new Coordenada('B',3);
		Coordenada D3 = new Coordenada('D',3);
		Coordenada A4 = new Coordenada('A',4);
		Coordenada E6 = new Coordenada('E',6);
		Pieza P = new Peon(Color.BLANCO);	
		Pieza P1 = new Peon(Color.BLANCO);	
		Pieza P2 = new Peon(Color.BLANCO);	
		Pieza P3 = new Torre(Color.BLANCO);	
		Pieza P4 = new Dama(Color.BLANCO);	
		Pieza P5 = new Alfil(Color.BLANCO);
		Coordenada D4 = new Coordenada('D',4);
		Pieza p = new Peon(Color.NEGRO);
		T.colocaPiezaAt(C4, P);
		piezas.add(P);
		piezas.add(P1);
		piezas.add(P2);
		T.colocaPiezaAt(B3, P1);
		System.out.println(PartidaAjedrez.getInstancia().getPiezas(Color.BLANCO));
		T.colocaPiezaAt(D3, P2);
		piezas.add(P3);
		T.colocaPiezaAt(A4, P3);
		T.colocaPiezaAt(D4, P4);
		piezas.add(P4);
		T.colocaPiezaAt(E6, P5);
		//piezas.add(P5);
		System.out.println(paj.getInstancia().toString());
		System.err.println(E.casillasAmenazadasPorUnColor(paj.getInstancia().getTablero(), Color.BLANCO) + " PUNTOS DE CASILLAS AMENAZADAS");
	}

	@Test
	public void testPiezasDefendidas() throws ExcepcionCoordenadaErronea {
		List<Pieza> piezas = paj.getInstancia().piezas;
		Coordenada C4 = new Coordenada('C',4);
		Coordenada B3 = new Coordenada('B',3);
		Coordenada D3 = new Coordenada('D',3);
		Coordenada A4 = new Coordenada('A',4);
		Coordenada E6 = new Coordenada('E',6);
		Pieza P = new Peon(Color.BLANCO);	
		Pieza P1 = new Peon(Color.BLANCO);	
		Pieza P2 = new Peon(Color.BLANCO);	
		Pieza P3 = new Torre(Color.BLANCO);	
		Pieza P4 = new Dama(Color.BLANCO);	
		Pieza P5 = new Alfil(Color.BLANCO);
		Coordenada D4 = new Coordenada('D',4);
		Pieza p = new Peon(Color.NEGRO);
		T.colocaPiezaAt(C4, P);
		piezas.add(P);
		piezas.add(P1);
		piezas.add(P2);
		T.colocaPiezaAt(B3, P1);
		System.out.println(PartidaAjedrez.getInstancia().getPiezas(Color.BLANCO));
		T.colocaPiezaAt(D3, P2);
		piezas.add(P3);
		T.colocaPiezaAt(A4, P3);
		T.colocaPiezaAt(D4, P4);
		piezas.add(P4);
		T.colocaPiezaAt(E6, P5);
		piezas.add(P5);
		System.out.println(paj.getInstancia().toString());
		System.err.println(E.piezasDefendidas(paj, Color.BLANCO));
	}

	/**
	 * Test peones avanzados.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public void testPeonesAvanzados() throws ExcepcionCoordenadaErronea {

		Coordenada C4 = new Coordenada('H',3);
		Pieza P = new Peon(Color.BLANCO);	
		Coordenada D4 = new Coordenada('A',6);
		Pieza p = new Peon(Color.NEGRO);
		Pieza P5 = new Alfil(Color.BLANCO);
		
		T.colocaPiezaAt(D4, P);
		T.colocaPiezaAt(C4, p);
		System.err.println("PEones AVANZADOS BLANCO" + E.peonesAvanzados(P));
		System.err.println("PEones AVANZADOS NEGRO" + E.peonesAvanzados(p));
	//	System.out.println(paj.getInstancia().toString());
	}
}
