/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * The Class TestPruebas.
 *
 * @author Almira Molla, Alejandro
 */
public class TestPruebas {
    
    /** The f. */
    FactoriaPieza f;
    
    /** The paj. */
    PartidaAjedrez paj;
    
    /** The tab. */
    Tablero tab;
    
    

    @Before
    public void setUp() {
        f = new FactoriaPieza();
        paj = PartidaAjedrez.getInstancia();
        tab = paj.getTablero();
    }
    
   /* @Test
    public void creaPiezasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {
        System.out.println("creaPiezaTest");
		paj.colocaPieza("Pb G8");
        paj.colocaPieza("Pn D1");
        paj.colocaPieza("Pb D5");
        paj.colocaPieza("Pb A5");
		paj.colocaPieza("Pn H5");
		paj.colocaPieza("Pn A8");
		paj.colocaPieza("Pn H8");
		paj.colocaPieza("Pn H7");
        paj.colocaPieza("Rb A1");
        paj.colocaPieza("Rn H1");
        paj.colocaPieza("Rb E6");
  //      paj.colocaPieza("Pn D3");
		paj.colocaPieza("Pb C1");
		System.out.println(tab);
		Coordenada E6 = new Coordenada ('E',6);
	//	tab.getCasillaAt(E6).quitaPieza();
    }*/


   @Test
    public void factoriaTest() throws ExcepcionPiezaDesconocida {
        
		System.out.println("factoriaTest");
        Pieza rb = new Rey(Color.BLANCO);
        Pieza rn = new Rey(Color.NEGRO);
        Pieza pb = new Peon(Color.BLANCO);
        Pieza pn = new Peon(Color.NEGRO);
        
        assertEquals("Crea un reyb", rb, FactoriaPieza.creaPieza("Rey", Color.BLANCO));
        assertEquals("Crea un reyn", rn, f.creaPieza('R', Color.NEGRO));
        assertEquals("Crea un peonb", pb, f.creaPieza('P', Color.BLANCO));
        assertEquals("Crea un peonn", pn, f.creaPieza('P', Color.NEGRO));
        
    }
    

    @Test
    public void getPiezasTest() throws ExcepcionCoordenadaErronea {
		System.out.println("getPiezasTest: HAY " + paj.getPiezas(Color.BLANCO).size());
        System.out.println(paj.getPiezas(Color.BLANCO));
    }
    
 
    @Test
    public void peonIsEnUltimaFilaTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida  {
       
		System.out.println("peonIsEnUltimaFilaTest");
		
        Coordenada g8 = new Coordenada('G', 8);
        Coordenada d1 = new Coordenada('D', 1);
        Coordenada d5 = new Coordenada('D', 5);
        
        Peon pb1 = (Peon) paj.getPiezaAt(g8);
        Peon pn2 = (Peon) paj.getPiezaAt(d1);
        Peon pb2 = (Peon) paj.getPiezaAt(d5);
        
        //System.out.println(pb1);
        
        assertTrue("PeonB en ultimaFila",pb1.isEnUltimaFila());
		assertTrue("PeonN en ultimaFila",pn2.isEnUltimaFila());
        assertFalse("PeonB no en ultimaFila",pb2.isEnUltimaFila());
        
    }
    
 
    @Test
    public void FilasCasillasAmenazadasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {
        
        
        Coordenada cor1 = new Coordenada('H', 8);
        Casilla cas1 = new Casilla(Color.NEGRO, cor1);
        
        //paj.colocaPieza("Pb D5");
        
        System.out.println("FilasCasillasAmenazadasTest desde: "+cor1 + "    " + (char)('A'-1));
        System.out.print("N: ");
        for(int i=0; i<tab.getColumnaArriba(cas1).size();i++){
            System.out.print(tab.getColumnaArriba(cas1).get(i).getCoordenada() +" ");
        }
      
        System.out.println();
        System.out.print("S: ");
        for(int i=0; i<tab.getColumnaAbajo(cas1).size();i++){
            System.out.print(tab.getColumnaAbajo(cas1).get(i).getCoordenada() +" ");
        }

        System.out.print("O: ");
        for(int i=0; i<tab.getFilaIzquierda(cas1).size();i++){
            System.out.print(tab.getFilaIzquierda(cas1).get(i).getCoordenada() +" ");
        }
        
        System.out.println();
        System.out.print("E: ");
        for(int i=0; i<tab.getFilaDerecha(cas1).size();i++){
            System.out.print(tab.getFilaDerecha(cas1).get(i).getCoordenada() +" ");
        }
        System.out.println();
        //assertEquals("[Casilla [color=BLANCO, pieza=null, coordenada=C4], Casilla [color=NEGRO, pieza=null, coordenada=D4], Casilla [color=BLANCO, pieza=null, coordenada=E4], Casilla [color=NEGRO, pieza=Peon [getTipo()=P, getColor()=BLANCO, getCoordenada()=F4], coordenada=F4]]",tab.getFilaDerecha(cas1).toString());
    }
    
 
    @Test
    public void DiagonalCasillasAmenazadasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {
        
        
        Coordenada cor1 = new Coordenada('D', 5);
        Casilla cas1 = new Casilla(Color.NEGRO, cor1);
        
        System.out.println("DiagonalCasillasAmenazadasTest desde: "+cor1);
        System.out.print("NO: ");
        for(int i=0; i<tab.getDiagonalNO(cas1).size();i++){
            System.out.print(tab.getDiagonalNO(cas1).get(i).getCoordenada() +" ");
        }
        System.out.println("");
        
        System.out.println();
        System.out.print("NE: ");
        for(int i=0; i<tab.getDiagonalNE(cas1).size();i++){
            System.out.print(tab.getDiagonalNE(cas1).get(i).getCoordenada() +" ");
        }
        
        System.out.println();
        System.out.print("SO: ");
        for(int i=0; i<tab.getDiagonalSO(cas1).size();i++){
            System.out.print(tab.getDiagonalSO(cas1).get(i).getCoordenada() +" ");
        }
        System.out.println("");
       
        System.out.println();
        System.out.print("SE: ");
        for(int i=0; i<tab.getDiagonalSE(cas1).size();i++){
            System.out.print(tab.getDiagonalSE(cas1).get(i).getCoordenada() +" ");
        }
        System.out.println(); 
        //assertEquals("[Casilla [color=BLANCO, pieza=null, coordenada=C4], Casilla [color=NEGRO, pieza=null, coordenada=D4], Casilla [color=BLANCO, pieza=null, coordenada=E4], Casilla [color=NEGRO, pieza=Peon [getTipo()=P, getColor()=BLANCO, getCoordenada()=F4], coordenada=F4]]",tab.getFilaDerecha(cas1).toString());
    }
    
	/**
	 * Gets the saltos caballo test.
	 *
	 * @return the saltos caballo test
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
    public void getSaltosCaballoTest() throws ExcepcionCoordenadaErronea{
        
        Coordenada cor1 = new Coordenada('C', 3);
        Casilla cas1 = new Casilla(Color.NEGRO, cor1);
        System.out.println("getSaltosCaballoTest desde:" + cor1);
        for(int i = 0; i<tab.getSaltosCaballo(cas1).size();i++){
            System.out.print(tab.getSaltosCaballo(cas1).get(i).getCoordenada()+ " ");
        }
        System.out.println();
    }
    
    /**
     * Peonget casillas amenazadas test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
     * @throws ExcepcionPosicionNoValida the excepcion posicion no valida
     */
    @Test
    public void peongetCasillasAmenazadasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {
        
        System.out.print("peongetCasillasAmenazadasTest desde: ");
        Coordenada a5 = new Coordenada('A',5);
		System.out.println(a5+" "+paj.getPiezaAt(a5).getColor());
        System.out.println("Deberia ser: B6");
        System.out.println(paj.getPiezaAt(a5).getCasillasAmenazadas());
		Coordenada h7 = new Coordenada('H',7);
        System.out.println(h7+" "+paj.getPiezaAt(h7).getColor());
		System.out.println("Deberia ser: G6");
        System.out.println(paj.getPiezaAt(h7).getCasillasAmenazadas());
		Coordenada a8 = new Coordenada('A',8);
        System.out.println(a8+" "+paj.getPiezaAt(a8).getColor());
		System.out.println("Deberia ser: B7");
        System.out.println(paj.getPiezaAt(a8).getCasillasAmenazadas());
		Coordenada h8 = new Coordenada('H',8);
        System.out.println(h8+" "+paj.getPiezaAt(h8).getColor());
		System.out.println("Deberia ser: G7");
        System.out.println(paj.getPiezaAt(h8).getCasillasAmenazadas());
        Coordenada d5 = new Coordenada('D',5);
        System.out.println(d5+" "+paj.getPiezaAt(d5).getColor());
		System.out.println("Deberia ser: C6 E6");
        System.out.println(paj.getPiezaAt(d5).getCasillasAmenazadas());
        Coordenada g8 = new Coordenada('G',8);
        System.out.println(g8+" "+paj.getPiezaAt(g8).getColor());
		System.out.println("Deberia ser: []");
        System.out.println(paj.getPiezaAt(g8).getCasillasAmenazadas());
    }
	
    /**
     * Reyget casillas amenazadas test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
     * @throws ExcepcionPosicionNoValida the excepcion posicion no valida
     */
    @Test
    public void reygetCasillasAmenazadasTest() throws ExcepcionCoordenadaErronea, ExcepcionPiezaDesconocida, ExcepcionPosicionNoValida {
        
        System.out.println("reygetCasillasAmenazadasTest desde: ");
        Coordenada a1 = new Coordenada('A',1);
		System.out.println(a1+" "+paj.getPiezaAt(a1).getColor());
        System.out.println("Deberia ser: A2 B2 B1");
        System.out.println(paj.getPiezaAt(a1).getCasillasAmenazadas());
		Coordenada h1 = new Coordenada('H',1);
        System.out.println(h1+" "+paj.getPiezaAt(h1).getColor());
		System.out.println("Deberia ser: G1 G2 H2");
        System.out.println(paj.getPiezaAt(h1).getCasillasAmenazadas());
        Coordenada e6 = new Coordenada('E',6);
        System.out.println(e6+" "+paj.getPiezaAt(e6).getColor());
		System.out.println("Deberia ser: E7 F7 F6 F5 E5 D5 D6 D7");
        System.out.println(paj.getPiezaAt(e6).getCasillasAmenazadas());		
    }
    
    /**
     * Gets the amenazas test.
     *
     * @return the amenazas test
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     */
    @Test
    public void getAmenazasTest() throws ExcepcionCoordenadaErronea{
        System.out.println("getAmenazasTest");
        Coordenada cor = new Coordenada('B', 7);
        Casilla cas = tab.getCasillaAt(cor);
        System.out.println("Amenazo: "+cor+" con las piezas BLANCAS");
        System.out.println(tab.getAmenazas(cas, Color.BLANCO));
        
        System.out.println();
        System.out.println();

    }
    
    /**
     * Checks if is amenazada test.
     *
     * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
     */
    @Test
    public void isAmenazadaTest() throws ExcepcionCoordenadaErronea {
    	System.out.println("IsAmenazadaTest");
    	Coordenada cor1 = new Coordenada ('G',1);

    	Casilla cas = tab.getCasillaAt(cor1);
    	System.out.println("PIEZA" + cas.getPieza());
    	System.out.print("ESTA AMENAZADA: ");
    	System.out.println(tab.getCasillaAt(cor1).isAmenazada(Color.NEGRO));
    }
    }
