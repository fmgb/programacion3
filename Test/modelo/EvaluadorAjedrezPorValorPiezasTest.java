package modelo;

import static org.junit.Assert.*;

import modelo.excepciones.ExcepcionCoordenadaErronea;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * The Class EvaluadorAjedrezPorValorPiezasTest.
 */
public class EvaluadorAjedrezPorValorPiezasTest {

	/** The paj. */
	PartidaAjedrez paj;
	
	/** The t. */
	Tablero T;
	
	/** The e. */
	EvaluadorAjedrezPorValorPiezas E = new EvaluadorAjedrezPorValorPiezas();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		paj= PartidaAjedrez.getInstancia();
		T = paj.getTablero();

		paj.inicializaTablero();
		System.out.println(T.toString());
	}

	/**
	 * Test evalua.
	 *
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 */
	@Test
	public void testEvalua() throws ExcepcionCoordenadaErronea {
		System.out.println(E.evalua(paj, Color.BLANCO));
		
		T.getCasillaAt('A', 1).quitaPieza();
		System.out.println(E.evalua(paj,Color.BLANCO));
	}

/*	@Test
	public void testEvaluaMovimiento() {
		fail("Not yet implemented");
	}*/

}
