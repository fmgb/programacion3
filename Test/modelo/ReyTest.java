package modelo;

import static org.junit.Assert.*;

import modelo.excepciones.ExcepcionCoordenadaErronea;
import modelo.excepciones.ExcepcionPiezaDesconocida;
import modelo.excepciones.ExcepcionPosicionNoValida;

import org.junit.Before;
import org.junit.Test;

/**
 * The Class ReyTest.
 */
public class ReyTest{
	
	/** The f. */
	FactoriaPieza f;
	
	/** The paj. */
	PartidaAjedrez paj;
	
	/** The tab. */
	Tablero tab;
	
	/** The p. */
	Pieza T,Tn,P;
	
	/** The r. */
	Rey R;
	
	/** The B6. */
	Coordenada E1,A1,B1,C1,A2,B2,C2,D2,B6;
	
	/** The pb4. */
	Peon pn1,pn2,pn3,pn4,pb1,pb2,pb3,pb4;
	  
  	/**
  	 * Sets the up.
  	 *
  	 * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
  	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
  	 * @throws ExcepcionPosicionNoValida the excepcion posicion no valida
  	 */
  	@Before
	    public void setUp() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
	        f = new FactoriaPieza();
	        paj = PartidaAjedrez.getInstancia();
	        tab = paj.getTablero();
	        E1 = new Coordenada('E',1);
	        A1 = new Coordenada('A',1);
	        B1 = new Coordenada('B',1);
	        C1 = new Coordenada('C',1);
	        B6 = new Coordenada('B',6);
	        A2 = new Coordenada('A', 2);
	        B2 = new Coordenada('B', 2);
	        C2 = new Coordenada('C', 2);
	        D2 = new Coordenada('D',2);
	        
/*	        pn1 = new Peon(Color.NEGRO);
	        pn2 = new Peon(Color.NEGRO);
	        pn3 = new Peon(Color.NEGRO);
	        pn4 = new Peon(Color.NEGRO);
	        pb1 = new Peon(Color.BLANCO);
	        pb2 = new Peon(Color.BLANCO);
	        pb3 = new Peon(Color.BLANCO);
	        pb4 = new Peon(Color.BLANCO);*/
	        T = FactoriaPieza.creaPieza('T', Color.BLANCO);
	        Tn = FactoriaPieza.creaPieza('T', Color.NEGRO);
	        R = new Rey(Color.BLANCO);
	        tab.colocaPiezaAt(B6, Tn);
	        P = FactoriaPieza.creaPieza('P', Color.BLANCO);
	        tab.colocaPiezaAt(E1, R);
	        tab.colocaPiezaAt(A1, T);
	        System.out.println(tab.toString());

	  }

	/**
	 * Test comprobar enroque.
	 *
	 * @throws ExcepcionPiezaDesconocida the excepcion pieza desconocida
	 * @throws ExcepcionCoordenadaErronea the excepcion coordenada erronea
	 * @throws ExcepcionPosicionNoValida the excepcion posicion no valida
	 */
	@Test
	public void testComprobarEnroque() throws ExcepcionPiezaDesconocida, ExcepcionCoordenadaErronea, ExcepcionPosicionNoValida {
		assertTrue("Sin ningún impedimento",R.comprobarEnroque(T.getCasilla()));
        paj.colocaPieza("Pn C2");
		assertFalse("Amenaza al Rey en E1",R.comprobarEnroque(T.getCasilla()));
		paj.getPiezaAt(tab.getCasillaAt('C', 2).getCoordenada()).quitaDeCasilla();
		paj.colocaPieza("Pb B1");
		assertFalse("Pieza Blanca en B1",R.comprobarEnroque(T.getCasilla()));
		//paj.getPiezaAt(tab.getCasillaAt('B', 1).getCoordenada()).quitaDeCasilla();
		//paj.colocaPieza("Pb C1");
		//assertFalse("Pieza Blanca en C1", R.comprobarEnroque(T.getCasilla()));
		//paj.getPiezaAt(tab.getCasillaAt('C', 1).getCoordenada()).quitaDeCasilla();
		//paj.colocaPieza("Pn A2");
	//	assertTrue("Pieza Negra en A1", R.comprobarEnroque(T.getCasilla()));
	//	paj.getPiezaAt(tab.getCasillaAt('A', 2).getCoordenada()).quitaDeCasilla();
	//	paj.colocaPieza("Tn B6");
		assertFalse("Torre Negra en B6", R.comprobarEnroque(T.getCasilla()));
	//System.out.println("Amenazadas " + tab.getCasillaAt('B', 6).getPieza().getCasillasAmenazadas());
	    System.out.println(tab.toString());
	    
	}

}
